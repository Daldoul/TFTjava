package gamer.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.AbstractDocument;

import admin.interfaces.formation.FormationShowDialog;
import dao.DAOForma_user;
import dao.DAOFormation;
import dao.DAOUtilisateur;
import entities.Forma_user;
import entities.Formation;
import entities.Utilisateur;
import filters.NumericAndLengthFilter;
import interfaces.BgBorder;
import interfaces.Login;

public class GamerFormationDialog extends JDialog implements ActionListener, KeyListener, MouseListener {
	private static final long serialVersionUID = 1L;
	
	
	
	
	JButton btn_retour;
	
	
	JTextField txtclenum;
	JTextField txtclelieux;
	JTextField txtcletype;
	
	JTextField txtcledebut;
	JTextField txtclefin;
	JTextField txtclecompetition;
	
	JTextField txtcleevenement;
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	JMenuItem ouvrirItem;
    
    
    
    NumericAndLengthFilter numericandlengthfilter = new NumericAndLengthFilter(11);
	
	
	
	
	DAOFormation daof = new DAOFormation();
	List<Formation> listFormation = new ArrayList<Formation>();
	
	
	String col[] = { "Id", "Nom", "Date Debut" , "Date Fin" , "Lieux" ,  "Type"};
	JTable table ;
	DefaultTableModel model;
	TableRowSorter<TableModel> filtre;
	
	
	@SuppressWarnings("serial")
	public GamerFormationDialog(Utilisateur gamer,DAOUtilisateur dao, GamerFrame parent){
		
		this.setTitle("Vos Sessions de Formations");
		this.setResizable(false);
		this.setBounds(100, 100, 1000, 700);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1000, 700);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis04.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		
		
		////
		listFormation = new ArrayList<Formation>();
		
		DAOForma_user daofu = new DAOForma_user();
		List<Forma_user> listformauser = new ArrayList<Forma_user>();
		listformauser = daofu.getForma_usersByUserId(gamer.getId());
		int t1 = listformauser.size();
		for(int i = 0;i<t1;i++ ){
			listFormation.add(daof.getFormationById(listformauser.get(i).getId_formation()));
		}
		
		
		int t = listFormation.size();
		
	    Object data[][] = new Object[t][6];
	      for(int i = 0;i<t;i++ ){
	    	 
	    	data[i][0] = ""+listFormation.get(i).getId();
	        data[i][1] = ""+listFormation.get(i).getNom();
	        data[i][2] = ""+sdf.format(listFormation.get(i).getDate_ouverture());
	        data[i][3] = ""+sdf.format(listFormation.get(i).getDate_cloture());
	        data[i][4] = ""+listFormation.get(i).getLieux();
	        data[i][5] = ""+listFormation.get(i).getType();
	        
	        
	      }
        
        model = new DefaultTableModel(data, col){
	    	  /*
	    	  @SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int column) {
	    	        Class returnValue;
	    	        if ((column >= 0) && (column < getColumnCount())) {
	    	          returnValue = getValueAt(0, column).getClass();
	    	        } else {
	    	          returnValue = Object.class;
	    	        }
	    	        return returnValue;
	    	      }
	    	  */
	      };
	      table = new JTable(model){
	    	  	
	              @SuppressWarnings({ "unchecked", "rawtypes" })
				public Class getColumnClass(int column){return getValueAt(0, column).getClass();}
	              public boolean isCellEditable(int row, int column) {return false;}
	              
	      };

	      table.setRowHeight(80);
	      table.setCellSelectionEnabled(false);
	      table.setFocusable(false);
	      table.setRowSelectionAllowed(true);
//	      table.setWrapStyleWord(true);
//        table.setLineWrap(true);
	      table.setShowGrid(true);
	      table.setShowVerticalLines(true);
	      
	      
	      filtre = new TableRowSorter<TableModel>(model);
	      
	      
	      table.setRowSorter(filtre);
	      
	      table.addMouseListener(this);
	      //table.addAncestorListener(this);
	      
	      JScrollPane panJtab = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	      //panJtab.setPreferredSize(new Dimension(1240, 550));
	      panJtab.setBounds(15, 50, 970, 570);
	      
	      panel.add(panJtab); 
	      
	      
			// les recherches
			
		    txtclenum = new JTextField();
		    txtclenum.addKeyListener(this);
		    ( (AbstractDocument) txtclenum.getDocument()).setDocumentFilter(numericandlengthfilter);
		    txtclenum.setBounds(15, 21, 96, 29);
			panel.add(txtclenum);
			txtclenum.setColumns(11);
			
		  	txtclelieux = new JTextField();
		  	txtclelieux.addKeyListener(this);
		  	txtclelieux.setBounds(111, 21, 95, 29);
			panel.add(txtclelieux);
			txtclelieux.setColumns(20);
		  	
		  	txtcletype = new JTextField();
		  	txtcletype.addKeyListener(this);
		  	txtcletype.setBounds(206, 21, 95, 29);
			panel.add(txtcletype);
			txtcletype.setColumns(20);
		  	
		  	txtcledebut = new JTextField();
		  	txtcledebut.addKeyListener(this);
		  	( (AbstractDocument) txtcledebut.getDocument()).setDocumentFilter(numericandlengthfilter);
		  	txtcledebut.setBounds(301, 21, 94, 29);
			panel.add(txtcledebut);
			txtcledebut.setColumns(11);
		  	
			txtclefin = new JTextField();
			txtclefin.addKeyListener(this);
			txtclefin.setBounds(395, 21, 95, 29);
			panel.add(txtclefin);
			txtclefin.setColumns(20);
		  	
		  	
		  	txtclecompetition = new JTextField();
		  	txtclecompetition.addKeyListener(this);
		  	( (AbstractDocument) txtclecompetition.getDocument()).setDocumentFilter(numericandlengthfilter);
		  	txtclecompetition.setBounds(490, 21, 95, 29);
			panel.add(txtclecompetition);
			txtclecompetition.setColumns(11);
		  	
		  	txtcleevenement = new JTextField();
		  	txtcleevenement.addKeyListener(this);
		  	txtcleevenement.setBounds(585, 21, 95, 29);
			panel.add(txtcleevenement);
			txtcleevenement.setColumns(20);
		  	
			/////
			
			
			
			
			//////
			btn_retour = new JButton("Retour");
			btn_retour.addActionListener(this);
			btn_retour.setForeground(Color.BLUE);
			btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_retour.setBounds(750, 630, 200, 29);
		    panel.add(btn_retour);
		    
		
		 // Create popup menu, attach popup menu listener
		    JPopupMenu popupMenu = new JPopupMenu("Menu");
		    
		    ouvrirItem = new JMenuItem("Ouvrir");ouvrirItem.addActionListener(this);
		    popupMenu.add(ouvrirItem);
		    
		    
		    table.setComponentPopupMenu(popupMenu);
		
		
		
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e1) {
		if (e1.getSource() == btn_retour) {dispose();}
		
		
		
		if (e1.getSource() == ouvrirItem) {
            int row = table.getSelectedRow();
            //int column = table.getSelectedColumn();
            Formation f = listFormation.get(row);
            // you can play more here to get that cell value and all
            new FormationShowDialog(f).setVisible(true);
            //this.dispose();
            
        }
		
	}


	@Override
	public void keyPressed(KeyEvent arg0) {
		
		
	}


	@Override
	public void keyReleased(KeyEvent e) {
		//numero
				if(e.getSource()==txtclenum){
					String text = txtclenum.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter(text,0)   );
			        }
					
				}
				////nom
				if(e.getSource()==txtclelieux){
					String text = txtclelieux.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,1)   );
			        }
					
				}
				////prenom
				if(e.getSource()==txtcletype){
					String text = txtcletype.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,2)   );
			        }
					
				}
				/////cin
				if(e.getSource()==txtcledebut){
					String text = txtcledebut.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter(text,3)   );
			        }
					
				}
				//adresse
				if(e.getSource()==txtclefin){
					String text = txtclefin.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,4)   );
			        }
					
				}
				//telephone
				if(e.getSource()==txtclecompetition){
					String text = txtclecompetition.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter(text,5)   );
			        }
					
				}
				//grade		
				if(e.getSource()==txtcleevenement){
					String text = txtcleevenement.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,6)   );
			        }
					
				}
				
	}


	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mousePressed(MouseEvent e2) {
		if(e2.getSource()==table){
			
			if (e2.getClickCount() == 2) {
	            JTable target = (JTable) e2.getSource();
	            int row = target.getSelectedRow();
	            //int column = target.getSelectedColumn();
	            Formation f = listFormation.get(row);
	            // you can play more here to get that cell value and all
	            new FormationShowDialog(f).setVisible(true);
	            //this.dispose();
	           
	            //System.out.println("cccccccc");
	        }
			
			
			if (e2.getButton()== MouseEvent.BUTTON3){
				table.setRowSelectionAllowed(true);
				table.setColumnSelectionAllowed(false);
				table.changeSelection(e2.getY()/table.getRowHeight(),0,false, false); 
			} 
		}
		
	}


	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
