package interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.text.AbstractDocument;

import dao.DAOClub;
import dao.DAOUtilisateur;
import entities.Club;
import entities.Utilisateur;
import filters.LengthFilter;
import filters.NumericAndLengthFilter;
import outils.FileChooser;

public class InscriptionFrame extends JDialog implements ActionListener, ItemListener {
	private static final long serialVersionUID = 1L;
	
	public static final int TAILLE_TAMPON = 10240; // 10 ko
	
	JLabel lblcestVide_Nom ;
    JLabel lblcestVide_Prenom;
    JLabel lblcestVide_NCIN;
    JLabel lblcestVide_Email;
    JLabel lblcestVide_Image;
    JLabel lblcestVide_Username;
    JLabel lblcestVide_Password;
    
    JLabel lblcestVide_Telephone;
    JLabel lblcestVide_Adresse;
	
    File f1;
	File dir;
	
	String chemin = "C:/imagesUtilisateurs/";
	
	ImageIcon iconExc = new ImageIcon(getClass().getResource("/images/Exception.png"));
	ImageIcon iconWar = new ImageIcon(getClass().getResource("/images/ERROR35.png"));
	ImageIcon iconSucc = new ImageIcon(getClass().getResource("/images/Succes.png"));
	
	JTextField textField_nom;
	JTextField textField_Prenom;
	JTextField textField_email;
	JTextField textField_NCIN;
	JTextField textField_Username;
	JTextField textField_Password;
	
	JTextField textField_Telephone;
	JTextField textField_Adresse;
	JTextField textField_Image;
	
	JTextField textField_;
	JTextArea textField_Description;
	
	JPasswordField passwordField_Password;
	JButton btn_Inscription;
	JButton btn_Retour;
	JButton btn_Select;
	
	JComboBox<String> comboBox;
	JComboBox<String> gamerComboBoxGrade;
	JComboBox<String> arbitreComboBoxGrade;
	JComboBox<String> gamerComboBoxAge;
	JComboBox<String> gamerComboBoxClub;
	
	DAOClub daoc ;
	List<Club> listClub;
	
	JPanel panel;
	
	public InscriptionFrame(){
		
		this.setTitle("TFT - Inscription");
		this.setResizable(false);
		this.setBounds(100, 100, 703, 700);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 703, 700);
		this.getContentPane().add(panel);
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis02.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
			     
		
		
		LengthFilter lengthfilter = new LengthFilter(20);
		LengthFilter lengthfilter200 = new LengthFilter(200);
		NumericAndLengthFilter numericandlengthfilter = new NumericAndLengthFilter(20);
		NumericAndLengthFilter numericandlengthfiltercin = new NumericAndLengthFilter(20);
		//AdresseFilter adressefilter = new AdresseFilter(5);
		     
		
		
		
			 JPanel panel_1 = new JPanel();
		     panel_1.setBorder(new LineBorder(new Color(204, 255, 255), 4));
		     panel_1.setBackground(new Color(0, 102, 153));
		     panel_1.setBounds(99, 20, 502, 70);
		     panel.add(panel_1);
		     panel_1.setLayout(null);
		     
		     JLabel lblInscri = new JLabel("Inscrivez vous");
		     lblInscri.setHorizontalAlignment(SwingConstants.CENTER);
		     lblInscri.setForeground(Color.WHITE);
		     lblInscri.setFont(new Font("Lucida Blackletter", Font.PLAIN, 26));
		     lblInscri.setBounds(6, 6, 490, 58);
		     panel_1.add(lblInscri);
		     
		     //
		     JLabel lblCategorie = new JLabel("Categorie:");
		     lblCategorie.setForeground(Color.WHITE);
		     lblCategorie.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		     lblCategorie.setBounds(123, 120, 85, 28);
		     panel.add(lblCategorie);
		     
		     String[] d = {"Administrateur", "Visiteur", "Respensable", "Medecin", "Joueur", "Arbitre"};
			 comboBox = new JComboBox<String>(d);
			 comboBox.addItemListener(this);
			 // comboBox.setModel(new DefaultComboBoxModel(new String[] {"Admin", "Normale", "Medecin", "Joueur", "Arbitre"}));
		     comboBox.setBounds(236, 120, 190, 28);
		     panel.add(comboBox);
		     
		     String[] gamerGrade = {"Amateur", "Pro"};
		     gamerComboBoxGrade = new JComboBox<String>(gamerGrade);
		     gamerComboBoxGrade.addItemListener(this);
		     gamerComboBoxGrade.setBounds(430, 120, 120, 28);
		     //panel.add(gamerComboBoxGrade);
		     
		     String[] arbitrerGrade = {"Amateur", "National", "International"};
		     arbitreComboBoxGrade = new JComboBox<String>(arbitrerGrade);
		     arbitreComboBoxGrade.addItemListener(this);
		     arbitreComboBoxGrade.setBounds(430, 120, 120, 28);
		     //panel.add(arbitreComboBoxGrade);
		     
		 	
		     
		     String[] gamerAge = {"Junior", "Senior", "V�t�rans"};
		     gamerComboBoxAge = new JComboBox<String>(gamerAge);
		     gamerComboBoxAge.addItemListener(this);
		     gamerComboBoxAge.setBounds(430, 158, 120, 28);
		     //panel.add(gamerComboBoxAge);
		     
		     //String[] gamerClub = {"111", "222","333", "444","555", "666"};
		     List<Club> listClub=new ArrayList<Club>();
		     
		     daoc = new DAOClub();
		     listClub = daoc.getClubs();
		     int t = listClub.size();
		     String data[] = new String[t];
			    //int i = 0;
			      for(int i = 0;i<t;i++ ){
			    	  data[i]=listClub.get(i).getNom();
			      }
		     
		     gamerComboBoxClub = new JComboBox<String>(data);
		     
		     
		     
		     
		     
		     gamerComboBoxClub.addItemListener(this);
		     gamerComboBoxClub.setBounds(430, 196, 120, 28);
		     //panel.add(gamerComboBoxClub);
		     
		     
		 	
		     
		     
		     
		     
		     
		     
		     //
		     JLabel lblNom = new JLabel("Nom:");
		     lblNom.setHorizontalAlignment(SwingConstants.RIGHT);
		     lblNom.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		     lblNom.setForeground(Color.WHITE);
		     lblNom.setBackground(Color.BLACK);
		     lblNom.setBounds(148, 158, 60, 28);
		     panel.add(lblNom);
		     textField_nom = new JTextField();
		     ( (AbstractDocument) textField_nom.getDocument()).setDocumentFilter(lengthfilter);
		     textField_nom.setBounds(236, 158, 190, 28);
		     panel.add(textField_nom);
		     textField_nom.setColumns(10);
		     /////
		     
		     ///// Prenom
		     JLabel lblPrenom = new JLabel("Prenom:");
		     lblPrenom.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		     lblPrenom.setForeground(Color.WHITE);
		     lblPrenom.setBounds(133, 196, 75, 28);
		     panel.add(lblPrenom);
		     textField_Prenom = new JTextField();
		     ( (AbstractDocument) textField_Prenom.getDocument()).setDocumentFilter(lengthfilter);
		     textField_Prenom.setBounds(236, 196, 190, 28);
		     panel.add(textField_Prenom);
		     textField_Prenom.setColumns(10);
		     /////
		     
		     
		     
		     ///// CIN
		     JLabel lblLogin = new JLabel("N� CIN:");
		     lblLogin.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		     lblLogin.setForeground(Color.WHITE);
		     lblLogin.setBounds(140, 234, 190, 28);
		     panel.add(lblLogin);
		     textField_NCIN = new JTextField();
		     ( (AbstractDocument) textField_NCIN.getDocument()).setDocumentFilter(numericandlengthfiltercin);
		     textField_NCIN.setBounds(236, 234, 190, 28);
		     panel.add(textField_NCIN);
		     textField_NCIN.setColumns(10);
		     ///////
		     
		     ////// Email
		     JLabel lblEmail = new JLabel("Email:");
		     lblEmail.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		     lblEmail.setForeground(Color.WHITE);
		     lblEmail.setBounds(155, 272, 190, 28);
		     panel.add(lblEmail);
		     
		     textField_email = new JTextField();
		     ( (AbstractDocument) textField_email.getDocument()).setDocumentFilter(lengthfilter200);
		     textField_email.setBounds(236, 272, 190, 28);
		     panel.add(textField_email);
		     textField_email.setColumns(10);
		     ///////
		     
		     ////// Telephone
		     JLabel lblTelephone = new JLabel("T�l�phone:");
		     lblTelephone.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		     lblTelephone.setForeground(Color.WHITE);
		     lblTelephone.setBounds(113, 310, 190, 28);
		     panel.add(lblTelephone);
		     
		     textField_Telephone = new JTextField();
		     ( (AbstractDocument) textField_Telephone.getDocument()).setDocumentFilter(numericandlengthfilter);
		     textField_Telephone.setBounds(236, 310, 190, 28);
		     panel.add(textField_Telephone);
		     textField_Telephone.setColumns(50);
		     ///////
		    
		     ////// Adresse
			JLabel lblAdresse = new JLabel("Adresse:");
			lblAdresse.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
			lblAdresse.setForeground(Color.WHITE);
			lblAdresse.setBounds(133, 348, 190, 28);
			panel.add(lblAdresse);
				     
			textField_Adresse = new JTextField();
			( (AbstractDocument) textField_Adresse.getDocument()).setDocumentFilter(lengthfilter);
			textField_Adresse.setBounds(236, 348, 190, 28);
			panel.add(textField_Adresse);
			textField_Adresse.setColumns(50);
			///////		     
		     
			 ////// username
			JLabel lblUsername = new JLabel("Username:");
			lblUsername.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
			lblUsername.setForeground(Color.WHITE);
			lblUsername.setBounds(116, 386, 94, 22);
			panel.add(lblUsername);
				     
			textField_Username = new JTextField();
			( (AbstractDocument) textField_Username.getDocument()).setDocumentFilter(lengthfilter);
			textField_Username.setBounds(236, 386, 190, 28);
			panel.add(textField_Username);
			textField_Username.setColumns(50);
			///////	
			
		     ///// Password
		     textField_Password = new JTextField();
		     textField_Password.setBounds(236, 424, 190, 28);
		     panel.add(textField_Password);
		     textField_Password.setColumns(10);
		     
		     JLabel lblPassword = new JLabel("Password:");
		     lblPassword.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		     lblPassword.setForeground(Color.WHITE);
		     lblPassword.setBounds(116, 424, 94, 22);
		     panel.add(lblPassword);
		     //////
		     
		     ///// description
		     JLabel lblDescription = new JLabel("Description:");
		     lblDescription.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		     lblDescription.setForeground(Color.WHITE);
		     lblDescription.setBounds(116, 462, 200, 22);
		     panel.add(lblDescription);
		    
		     textField_Description = new JTextArea();
		     textField_Description.setBounds(236, 462, 250, 100);
		     textField_Description.setLineWrap(true);
		     ( (AbstractDocument) textField_Username.getDocument()).setDocumentFilter(lengthfilter200);
		     panel.add(textField_Description);
		     textField_Description.setColumns(10);
		     
		     //////
		     
		     
		     
		     
		     
		     ///// image
		     JLabel lblIamge = new JLabel("Image:");
		     lblIamge.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		     lblIamge.setForeground(Color.WHITE);
		     lblIamge.setBounds(146, 572, 94, 22);
		     panel.add(lblIamge);
		     
		     textField_Image = new JTextField();
		     textField_Image.setEditable(false);
		     textField_Image.setBounds(236, 572, 190, 28);
		     panel.add(textField_Image);
		     textField_Image.setColumns(10);
		     
		     
		     btn_Select = new JButton("Parcourir");
		     btn_Select.addActionListener(this);
		     btn_Select.setForeground(Color.BLUE);
		     btn_Select.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		     btn_Select.setBounds(428, 572, 109, 29);
		     panel.add(btn_Select);
		     
		     //////
		     		     
		     //////////////
		     btn_Retour = new JButton("Retour");
		     btn_Retour.addActionListener(this);
		     btn_Retour.setForeground(Color.BLUE);
		     btn_Retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		     btn_Retour.setBounds(120, 630, 241, 29);
		     panel.add(btn_Retour);
		     
		     btn_Inscription = new JButton("Inscription");
		     btn_Inscription.addActionListener(this);
		     btn_Inscription.setForeground(Color.BLUE);
		     btn_Inscription.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		     btn_Inscription.setBounds(380, 630, 241, 29);
		     
		     panel.add(btn_Inscription);
		     
		     
		     
		     lblcestVide_Nom = new JLabel("*C'est vide");
		     lblcestVide_Nom.setForeground(Color.RED);
		     lblcestVide_Nom.setHorizontalAlignment(SwingConstants.RIGHT);
		     lblcestVide_Nom.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		     lblcestVide_Nom.setBounds(351, 145, 75, 16);
		     lblcestVide_Nom.setVisible(false);
		     panel.add(lblcestVide_Nom);
		     
		     lblcestVide_Prenom = new JLabel("*C'est vide");
		     lblcestVide_Prenom.setHorizontalAlignment(SwingConstants.RIGHT);
		     lblcestVide_Prenom.setForeground(Color.RED);
		     lblcestVide_Prenom.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		     lblcestVide_Prenom.setBounds(351, 183, 75, 16);
		     lblcestVide_Prenom.setVisible(false);
		     panel.add(lblcestVide_Prenom);
		     
		     lblcestVide_NCIN = new JLabel("*C'est vide");
		     lblcestVide_NCIN.setHorizontalAlignment(SwingConstants.RIGHT);
		     lblcestVide_NCIN.setForeground(Color.RED);
		     lblcestVide_NCIN.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		     lblcestVide_NCIN.setBounds(351, 220, 75, 16);
		     lblcestVide_NCIN.setVisible(false);
		     panel.add(lblcestVide_NCIN);
		     
		     lblcestVide_Email = new JLabel("*C'est vide");
		     lblcestVide_Email.setHorizontalAlignment(SwingConstants.RIGHT);
		     lblcestVide_Email.setForeground(Color.RED);
		     lblcestVide_Email.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		     lblcestVide_Email.setBounds(351, 259, 75, 16);
		     lblcestVide_Email.setVisible(false);
		     panel.add(lblcestVide_Email);
		     
		     
		     lblcestVide_Telephone = new JLabel("*C'est vide");
		     lblcestVide_Telephone.setHorizontalAlignment(SwingConstants.RIGHT);
		     lblcestVide_Telephone.setForeground(Color.RED);
		     lblcestVide_Telephone.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		     lblcestVide_Telephone.setBounds(351, 297, 75, 16);
		     lblcestVide_Telephone.setVisible(false);
		     panel.add(lblcestVide_Telephone);
		     
		     lblcestVide_Adresse = new JLabel("*C'est vide");
		     lblcestVide_Adresse.setHorizontalAlignment(SwingConstants.RIGHT);
		     lblcestVide_Adresse.setForeground(Color.RED);
		     lblcestVide_Adresse.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		     lblcestVide_Adresse.setBounds(351, 335, 75, 16);
		     lblcestVide_Adresse.setVisible(false);
		     panel.add(lblcestVide_Adresse);
		     
		     lblcestVide_Username = new JLabel("*C'est vide");
		     lblcestVide_Username.setHorizontalAlignment(SwingConstants.RIGHT);
		     lblcestVide_Username.setForeground(Color.RED);
		     lblcestVide_Username.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		     lblcestVide_Username.setBounds(351, 373, 75, 16);
		     lblcestVide_Username.setVisible(false);
		     panel.add(lblcestVide_Username);
		     
		     lblcestVide_Password = new JLabel("*C'est vide");
		     lblcestVide_Password.setHorizontalAlignment(SwingConstants.RIGHT);
		     lblcestVide_Password.setForeground(Color.RED);
		     lblcestVide_Password.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		     lblcestVide_Password.setBounds(351, 373, 75, 16);
		     lblcestVide_Password.setVisible(false);
		     panel.add(lblcestVide_Password);
		     
		     lblcestVide_Image = new JLabel("*Aucune Image s�l�ctionn�e");
		     lblcestVide_Image.setHorizontalAlignment(SwingConstants.RIGHT);
		     lblcestVide_Image.setForeground(Color.RED);
		     lblcestVide_Image.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		     lblcestVide_Image.setBounds(282, 420, 140, 16);
		     lblcestVide_Image.setVisible(false);
		     panel.add(lblcestVide_Image);
		     
		     
		
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e3) {
		
		
		if(e3.getSource() == btn_Inscription){
			
			//System.out.println("btn_Inscription event");
			 lblcestVide_Password.setVisible(false);
		     lblcestVide_Image.setVisible(false);
		     lblcestVide_Email.setVisible(false);
		     lblcestVide_Prenom.setVisible(false);
		     lblcestVide_Nom.setVisible(false);
		     lblcestVide_Adresse.setVisible(false);
		     lblcestVide_NCIN.setVisible(false);
		     lblcestVide_Telephone.setVisible(false);
		     lblcestVide_Username.setVisible(false);
		     
   		 if(textField_nom.getText().isEmpty()==false){
   			 if(textField_Prenom.getText().isEmpty()==false){
   				if(textField_NCIN.getText().isEmpty()==false){
	   				 if(textField_email.getText().isEmpty()==false){
	   					 if(textField_Telephone.getText().isEmpty()==false){
	   						if(textField_Adresse.getText().isEmpty()==false){
	   							if(textField_Username.getText().isEmpty()==false){
	   								if(textField_Password.getText().isEmpty()==false){
	   									if(textField_Image.getText().isEmpty()==false){
	   							 
	   							 String sComboBx = (String) comboBox.getSelectedItem();
	   								
	   							 // la dao
	   							
	   							 
	   							 
	   							 if(sComboBx.equals("Joueur")){
	   								DAOUtilisateur daoUtilisateur = new DAOUtilisateur();
	      							 Utilisateur u = new Utilisateur();
	      							 u.setRoles(sComboBx);
	   	   							 u.setNom(textField_nom.getText());
	   	   							 u.setPrenom(textField_Prenom.getText());
	   	   							 u.setCin(Integer.parseInt(textField_NCIN.getText()));
	   	   							 u.setEmail(textField_email.getText());
	   	   							 u.setTelephone(Integer.parseInt(textField_Telephone.getText()));
	   	   							 u.setAdresse(textField_Adresse.getText());
	   	   							 u.setUsername(textField_Username.getText());
	   	   							 u.setPassword(textField_Password.getText()); 
	   	   							 u.setDescription(textField_Description.getText());
	   	   							 u.setImage(chemin+textField_NCIN.getText()+".jpg");
	   	   							 
	   	   							 u.setGrade( (String) gamerComboBoxGrade.getSelectedItem()); 
	   	   							 u.setTranche_age( (String) gamerComboBoxAge.getSelectedItem()); 
	   	   							 
	   	   							 //int id_club = daoc.getClubByNom((String) gamerComboBoxClub.getSelectedItem());
	   	   							 
	   	   							 
	   	   							 int idc= daoc.getClubByNom((String) gamerComboBoxClub.getSelectedItem());
	   	   							 u.setId_club( idc); 
	   	   							 
	   	   							 // must be genereted by FOS
	   	   							 u.setUsername_canonical(textField_Username.getText()+Integer.parseInt(textField_NCIN.getText()));
	   	   							 u.setEmail_canonical(textField_email.getText()+Integer.parseInt(textField_NCIN.getText()));
	   	   							 byte b = 100;
	   	   							 u.setEnabled(b);
	   	   							 u.setSalt("salt");
	   	   							 u.setLocked(b);
	   	   							 u.setExpired(b);
	   	   							 ///
	   	   							
	   	   							 
	   	   							
	   									try {
	   										ecrireFichier(f1, textField_NCIN.getText()+".jpg", chemin);
	   									} catch (IOException e) {
	   										JOptionPane.showMessageDialog(null,"Erreur..", "erreur fichier image ",JOptionPane.INFORMATION_MESSAGE, iconSucc);
	   										e.printStackTrace();}
	   									
	   								 
	   	   							
	   	   							 			daoUtilisateur.inscrirJoueur(u);
	   	   							 
	   							 }
	   							 
	   							if(sComboBx.equals("Arbitre")){
	   								DAOUtilisateur daoUtilisateur = new DAOUtilisateur();
	      							 Utilisateur u = new Utilisateur();
	      							 u.setRoles(sComboBx);
	   	   							 u.setNom(textField_nom.getText());
	   	   							 u.setPrenom(textField_Prenom.getText());
	   	   							 u.setCin(Integer.parseInt(textField_NCIN.getText()));
	   	   							 u.setEmail(textField_email.getText());
	   	   							 u.setTelephone(Integer.parseInt(textField_Telephone.getText()));
	   	   							 u.setAdresse(textField_Adresse.getText());
	   	   							 u.setUsername(textField_Username.getText());
	   	   							 u.setPassword(textField_Password.getText()); 
	   	   							 u.setDescription(textField_Description.getText());
	   	   							 u.setImage(chemin+textField_NCIN.getText()+".jpg");
	   	   							 u.setGrade( (String) arbitreComboBoxGrade.getSelectedItem());
	   	   							 
	   	   							 
	   	   							 // must be genereted by FOS
	   	   							 u.setUsername_canonical(textField_Username.getText()+Integer.parseInt(textField_NCIN.getText()));
	   	   							 u.setEmail_canonical(textField_email.getText()+Integer.parseInt(textField_NCIN.getText()));
	   	   							 byte b = 100;
	   	   							 u.setEnabled(b);
	   	   							 u.setSalt("salt");
	   	   							 u.setLocked(b);
	   	   							 u.setExpired(b);
	   	   							 ///
	   	   							
	   	   							 
	   	   							
	   									try {
	   										ecrireFichier(f1, textField_NCIN.getText()+".jpg", chemin);
	   									} catch (IOException e) {
	   										JOptionPane.showMessageDialog(null,"Erreur..", "erreur fichier image ",JOptionPane.INFORMATION_MESSAGE, iconSucc);
	   										e.printStackTrace();}
	   									
	   								
	   	   							
	   	   							 			daoUtilisateur.inscrirArbitre(u);
	   	   							 
	   							}
	   							
	   							if(sComboBx.equals("Administrateur") || sComboBx.equals("Respensable")){
	   								DAOUtilisateur daoUtilisateur = new DAOUtilisateur();
	      							 Utilisateur u = new Utilisateur();
	      							 u.setRoles(sComboBx);
	   	   							 u.setNom(textField_nom.getText());
	   	   							 u.setPrenom(textField_Prenom.getText());
	   	   							 u.setCin(Integer.parseInt(textField_NCIN.getText()));
	   	   							 u.setEmail(textField_email.getText());
	   	   							 u.setTelephone(Integer.parseInt(textField_Telephone.getText()));
	   	   							 u.setAdresse(textField_Adresse.getText());
	   	   							 u.setUsername(textField_Username.getText());
	   	   							 u.setPassword(textField_Password.getText()); 
	   	   							 u.setDescription(textField_Description.getText());
	   	   							 u.setImage(chemin+textField_NCIN.getText()+".jpg");
	   	   							 
	   	   							 
	   	   							 
	   	   							 // must be genereted by FOS
	   	   							 u.setUsername_canonical(textField_Username.getText()+Integer.parseInt(textField_NCIN.getText()));
	   	   							 u.setEmail_canonical(textField_email.getText()+Integer.parseInt(textField_NCIN.getText()));
	   	   							 byte b = 100;
	   	   							 u.setEnabled(b);
	   	   							 u.setSalt("salt");
	   	   							 u.setLocked(b);
	   	   							 u.setExpired(b);
	   	   							 ///
	   	   							
	   	   							 
	   	   							
	   									try {
	   										ecrireFichier(f1, textField_NCIN.getText()+".jpg", chemin);
	   									} catch (IOException e) {
	   										JOptionPane.showMessageDialog(null,"Erreur..", "erreur fichier image ",JOptionPane.INFORMATION_MESSAGE, iconSucc);
	   										e.printStackTrace();}
	   									
	   								
	   	   							
	   	   							 			daoUtilisateur.ajouterUtilisateur(u);
	   	   							 
	   							}
	   							 
	   							if( sComboBx.equals("Visiteur")){
	   								DAOUtilisateur daoUtilisateur = new DAOUtilisateur();
	      							 Utilisateur u = new Utilisateur();
	      							 u.setRoles(sComboBx);
	   	   							 u.setNom(textField_nom.getText());
	   	   							 u.setPrenom(textField_Prenom.getText());
	   	   							 u.setCin(Integer.parseInt(textField_NCIN.getText()));
	   	   							 u.setEmail(textField_email.getText());
	   	   							 u.setTelephone(Integer.parseInt(textField_Telephone.getText()));
	   	   							 u.setAdresse(textField_Adresse.getText());
	   	   							 u.setUsername(textField_Username.getText());
	   	   							 u.setPassword(textField_Password.getText()); 
	   	   							 u.setDescription(textField_Description.getText());
	   	   							 u.setImage(chemin+textField_NCIN.getText()+".jpg");
	   	   							 
	   	   							 
	   	   							 
	   	   							 // must be genereted by FOS
	   	   							 u.setUsername_canonical(textField_Username.getText()+Integer.parseInt(textField_NCIN.getText()));
	   	   							 u.setEmail_canonical(textField_email.getText()+Integer.parseInt(textField_NCIN.getText()));
	   	   							 byte b = 100;
	   	   							 u.setEnabled(b);
	   	   							 u.setSalt("salt");
	   	   							 u.setLocked(b);
	   	   							 u.setExpired(b);
	   	   							 ///
	   	   							
	   	   							 
	   	   							
	   									try {
	   										ecrireFichier(f1, textField_NCIN.getText()+".jpg", chemin);
	   									} catch (IOException e) {
	   										JOptionPane.showMessageDialog(null,"Erreur..", "erreur fichier image ",JOptionPane.INFORMATION_MESSAGE, iconSucc);
	   										e.printStackTrace();}
	   									
	   								
	   	   							
	   	   							 			daoUtilisateur.InscrireVisiteur(u);
	   	   							 
	   							}
	   							
	   							if(sComboBx.equals("Medecin") ){
	   								DAOUtilisateur daoUtilisateur = new DAOUtilisateur();
	      							 Utilisateur u = new Utilisateur();
	      							 u.setRoles(sComboBx);
	   	   							 u.setNom(textField_nom.getText());
	   	   							 u.setPrenom(textField_Prenom.getText());
	   	   							 u.setCin(Integer.parseInt(textField_NCIN.getText()));
	   	   							 u.setEmail(textField_email.getText());
	   	   							 u.setTelephone(Integer.parseInt(textField_Telephone.getText()));
	   	   							 u.setAdresse(textField_Adresse.getText());
	   	   							 u.setUsername(textField_Username.getText());
	   	   							 u.setPassword(textField_Password.getText()); 
	   	   							 u.setDescription(textField_Description.getText());
	   	   							 u.setImage(chemin+textField_NCIN.getText()+".jpg");
	   	   							 
	   	   							 
	   	   							 
	   	   							 // must be genereted by FOS
	   	   							 u.setUsername_canonical(textField_Username.getText()+Integer.parseInt(textField_NCIN.getText()));
	   	   							 u.setEmail_canonical(textField_email.getText()+Integer.parseInt(textField_NCIN.getText()));
	   	   							 byte b = 100;
	   	   							 u.setEnabled(b);
	   	   							 u.setSalt("salt");
	   	   							 u.setLocked(b);
	   	   							 u.setExpired(b);
	   	   							 ///
	   	   							
	   	   							 
	   	   							
	   									try {
	   										ecrireFichier(f1, textField_NCIN.getText()+".jpg", chemin);
	   									} catch (IOException e) {
	   										JOptionPane.showMessageDialog(null,"Erreur..", "erreur fichier image ",JOptionPane.INFORMATION_MESSAGE, iconSucc);
	   										e.printStackTrace();}
	   									
	   								
	   	   							
	   	   							 			daoUtilisateur.ajouterMedecin(u);
	   	   							 
	   							}
	   							 
	   								JOptionPane.showMessageDialog(null,
	   										"Success..!!"+
	   										"\nComboBox: "+sComboBx+
	   										"\nNom: "+textField_nom.getText()+
	   										"\nPronom: " +textField_Prenom.getText()+
	   										"\nemail: "+textField_email.getText()+
	   										"\nUser Name: "+textField_NCIN.getText()+
	   										"\nPassword: "+textField_Password.getText()
	   										, "Success: ",
	   										JOptionPane.INFORMATION_MESSAGE, iconSucc);
	   								textField_nom.setEditable(false);
	   								textField_Prenom.setEditable(false);
	   								textField_email.setEditable(false);
	   								textField_NCIN.setEditable(false);
	   								textField_Password.setEditable(false);
	   								
	   								this.dispose();
	   						}else{//Password is empty
	   							 lblcestVide_Image.setVisible(true);
								     JOptionPane.showMessageDialog(null,
												"Aucune Image s�l�ctionn�e", "Warning:",
												JOptionPane.WARNING_MESSAGE, iconWar);
								     //textField_Password.requestFocus();
	   						 } 		    							 
	   						 }else{//Password is empty
	   							 lblcestVide_Password.setVisible(true);
								     JOptionPane.showMessageDialog(null,
												"Password c'est vide", "Warning:",
												JOptionPane.WARNING_MESSAGE, iconWar);
								     textField_Password.requestFocus();
	   						 }
	   						 
	   							}else{//User userName is empty
			   						 lblcestVide_Username.setVisible(true);
									     JOptionPane.showMessageDialog(null,
													"Username c'est vide", "Warning:",
													JOptionPane.WARNING_MESSAGE, iconWar);
									     textField_Username.requestFocus();
			   					 }		
	   								
	   						}else{//User Name is empty
		   						 lblcestVide_Adresse.setVisible(true);
								     JOptionPane.showMessageDialog(null,
												"Adresse c'est vide", "Warning:",
												JOptionPane.WARNING_MESSAGE, iconWar);
								     textField_Adresse.requestFocus();
		   					 }
	   						 
	   					 }else{//User Name is empty
	   						 lblcestVide_Telephone.setVisible(true);
							     JOptionPane.showMessageDialog(null,
											"T�l�phone c'est vide", "Warning:",
											JOptionPane.WARNING_MESSAGE, iconWar);
							     textField_Telephone.requestFocus();
	   					 }
	   				 }else{//Email is Empty
	   					 lblcestVide_Email.setVisible(true);
						     JOptionPane.showMessageDialog(null,
										"Email c'est vide", "Warning:",
										JOptionPane.WARNING_MESSAGE, iconWar);
						     textField_email.requestFocus();
	   				 }
	   				 
		   			 }else{//Prenom is Empty
		   				 lblcestVide_NCIN.setVisible(true);
						     JOptionPane.showMessageDialog(null,
										"N�CIN c'est vide", "Warning:",
										JOptionPane.WARNING_MESSAGE, iconWar);
						     textField_NCIN.requestFocus();
		   			 }
	   				 
	   			 }else{//Prenom is Empty
	   				 lblcestVide_Prenom.setVisible(true);
					     JOptionPane.showMessageDialog(null,
									"Prenom c'est vide", "Warning:",
									JOptionPane.WARNING_MESSAGE, iconWar);
					     textField_Prenom.requestFocus();
	   			 }
	   		 }else{//nom is empy
				     lblcestVide_Nom.setVisible(true);
				     System.out.println("nom vide");
				     JOptionPane.showMessageDialog(null,
								"Nom c'est vide", "Warning:",
								JOptionPane.WARNING_MESSAGE, iconWar);
				     textField_nom.requestFocus();
	   		 }
				
			}	
		
		
		if(e3.getSource() == btn_Select){
			FileChooser fc = new FileChooser(this);
			if(fc.getSelectedFile()!=null){
			f1=fc.getSelectedFile();
			textField_Image.setText("");
			textField_Image.setText(f1.getName());
			dir=fc.getCurrentDirectory();
		}
		}	
			
		
		if(e3.getSource() == btn_Retour){
			dispose();
		}
			

	
	}
	
	
	private void ecrireFichier( File f, String nomFichier, String chemin ) throws IOException {
	    /* Pr�pare les flux. */
	    BufferedInputStream entree = null;
	    BufferedOutputStream sortie = null;
	    
	    System.out.println(f.getPath());
		System.out.println(f.getName());
		System.out.println("nomFichier = "+nomFichier);
		System.out.println("chemin = "+chemin);
	    try {
	        /* Ouvre les flux. */
	    	
	    	entree = new BufferedInputStream( new FileInputStream( f ), TAILLE_TAMPON );
	        sortie = new BufferedOutputStream( new FileOutputStream( new File( chemin + nomFichier ) ), TAILLE_TAMPON );
	        /*
	         * Lit le fichier re�u et �crit son contenu dans un fichier sur le
	         * disque.
	         */
	        byte[] tampon = new byte[TAILLE_TAMPON];
	        int longueur;
	        while ( ( longueur = entree.read( tampon ) ) > 0 ) {
	            sortie.write( tampon, 0, longueur );
	        }
	    } 
	    finally {
	        try {

	            sortie.close();
	        } catch ( IOException ignore ) {ignore.printStackTrace();}
	        try {
	            entree.close();
	        } catch ( IOException ignore ) {ignore.printStackTrace();}
	    }
	}

	@Override
	public void itemStateChanged(ItemEvent e3) {
		if(e3.getSource()==comboBox){
			String s = comboBox.getSelectedItem().toString();
			if(s.equals("Joueur")){
				
				panel.remove(arbitreComboBoxGrade);
				
				panel.add(gamerComboBoxGrade);
				panel.add(gamerComboBoxClub);
				panel.add(gamerComboBoxAge);
				
				SwingUtilities.updateComponentTreeUI(panel);
				
				
			}if(s.equals("Arbitre")){
				
				panel.remove(gamerComboBoxGrade);
				panel.remove(gamerComboBoxClub);
				panel.remove(gamerComboBoxAge);
				
				panel.add(arbitreComboBoxGrade);
				SwingUtilities.updateComponentTreeUI(panel);
			}if(s.equals("Administrateur") || s.equals("Visiteur") || s.equals("Medecin")){
				
				panel.remove(gamerComboBoxGrade);
				panel.remove(gamerComboBoxClub);
				panel.remove(gamerComboBoxAge);
				panel.remove(arbitreComboBoxGrade);
				
				SwingUtilities.updateComponentTreeUI(panel);
			}	
		}
		
	}
	
	
}
