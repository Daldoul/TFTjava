package interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import admin.interfaces.arbitre.ArbitreDialog;
import admin.interfaces.club.ClubDialog;
import admin.interfaces.competition.CompetitionDialog;
import admin.interfaces.event.EventDialog;
import admin.interfaces.formation.FormationDialog;
import admin.interfaces.gamer.GamerDialog;
import admin.interfaces.graph.GrapheDialog;
import admin.interfaces.match.MatchDialog;
import admin.interfaces.materiel.MaterielDialog;
import admin.interfaces.medecin.MedecinDialog;
import admin.interfaces.stade.StadeDialog;
import admin.interfaces.visitor.VisitorDialog;
import entities.Utilisateur;


public class AdminFrame extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	
	JButton btn_Arbitre;
	JButton btn_Gamer;
	JButton btn_Medecin;
	JButton btn_Visitor;
	
	JButton btn_Match;
	JButton btn_Competition;
	JButton btn_Session_Formation;
	JButton btn_Stade;
	
	JButton btn_Materiel;
	JButton btn_Autre;
	
	JButton btn_Event;
	JButton btn_Club;
	
	JButton btn_Exit;
	
	Utilisateur admin;
	
	public AdminFrame(Utilisateur u1){
		this.admin=u1;
		this.setTitle("TFT - Administration");
		this.setResizable(false);
		this.setBounds(100, 100, 1000, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1000, 600);
		this.getContentPane().add(panel);
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis003.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		
		JPanel panel_1 = new JPanel();
	     panel_1.setBorder(new LineBorder(new Color(204, 255, 255), 4));
	     panel_1.setBackground(new Color(0, 102, 153));
	     panel_1.setBounds(200, 60, 600, 70);
	     panel.add(panel_1);
	     panel_1.setLayout(null);
	     
	     JLabel lblInscri = new JLabel("Espace Administrateur de TFT - Bienvenue");
	     lblInscri.setHorizontalAlignment(SwingConstants.CENTER);
	     lblInscri.setForeground(Color.WHITE);
	     lblInscri.setFont(new Font("Lucida Blackletter", Font.PLAIN, 26));
	     lblInscri.setBounds(6, 6, 590, 58);
	     panel_1.add(lblInscri);
				
		btn_Arbitre = new JButton("Les Arbitres");
		btn_Arbitre.addActionListener(this);
		btn_Arbitre.setForeground(Color.BLUE);
		btn_Arbitre.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_Arbitre.setBounds(98, 271, 200, 29);
	    panel.add(btn_Arbitre);
	     
	    btn_Gamer = new JButton("Les Joueurs");
	    btn_Gamer.addActionListener(this);
	    btn_Gamer.setForeground(Color.BLUE);
	    btn_Gamer.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Gamer.setBounds(299, 271, 200, 29);
	    panel.add(btn_Gamer);
	    
	    
	    btn_Medecin = new JButton("Les Medecins");
	    btn_Medecin.addActionListener(this);
	    btn_Medecin.setForeground(Color.BLUE);
	    btn_Medecin.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Medecin.setBounds(500, 271, 200, 29);
	    panel.add(btn_Medecin);
		
	    btn_Visitor = new JButton("Les Visiteurs");
	    btn_Visitor.addActionListener(this);
	    btn_Visitor.setForeground(Color.BLUE);
	    btn_Visitor.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Visitor.setBounds(701, 271, 200, 29);
	    panel.add(btn_Visitor);
		
		
	    btn_Match = new JButton("Les Matchs");
	    btn_Match.addActionListener(this);
	    btn_Match.setForeground(Color.BLUE);
	    btn_Match.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Match.setBounds(98, 301, 200, 29);
	    panel.add(btn_Match);
	     
	    btn_Competition = new JButton("Les Comptetitions");
	    btn_Competition.addActionListener(this);
	    btn_Competition.setForeground(Color.BLUE);
	    btn_Competition.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Competition.setBounds(299, 301, 200, 29);
	    panel.add(btn_Competition);
	    
	    btn_Event = new JButton("Les Evenements");
	    btn_Event.addActionListener(this);
	    btn_Event.setForeground(Color.BLUE);
	    btn_Event.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Event.setBounds(500, 301, 200, 29);
	    panel.add(btn_Event);
	    
	    
	    btn_Session_Formation = new JButton("Les Sessions formation");
	    btn_Session_Formation.addActionListener(this);
	    btn_Session_Formation.setForeground(Color.BLUE);
	    btn_Session_Formation.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Session_Formation.setBounds(701, 301, 200, 29);
	    panel.add(btn_Session_Formation);
	    
	    
	    btn_Stade = new JButton("Les Sites / Stades");
	    btn_Stade.addActionListener(this);
	    btn_Stade.setForeground(Color.BLUE);
	    btn_Stade.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Stade.setBounds(98, 331, 200, 29);
	    panel.add(btn_Stade);
	    
	    
	    
	    btn_Materiel = new JButton("Les Materiels");
	    btn_Materiel.addActionListener(this);
	    btn_Materiel.setForeground(Color.BLUE);
	    btn_Materiel.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Materiel.setBounds(299, 331, 200, 29);
	    panel.add(btn_Materiel);
	    
	    btn_Club = new JButton("Les Clubs");
	    btn_Club.addActionListener(this);
	    btn_Club.setForeground(Color.BLUE);
	    btn_Club.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Club.setBounds(500, 331, 200, 29);
	    panel.add(btn_Club);
	    
	    btn_Autre = new JButton("Les Graphes");
	    btn_Autre.addActionListener(this);
	    btn_Autre.setForeground(Color.BLUE);
	    btn_Autre.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Autre.setBounds(701, 331, 200, 29);
	    panel.add(btn_Autre);
	    
	    
		
	    btn_Exit = new JButton("Quitter");
	    btn_Exit.addActionListener(this);
	    btn_Exit.setForeground(Color.BLUE);
	    btn_Exit.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Exit.setBounds(750, 519, 200, 29);
	    panel.add(btn_Exit);
	}
	

	public void actionPerformed(ActionEvent ev) {
		if (ev.getSource() == btn_Gamer){
			GamerDialog gamerDialog = new GamerDialog();
			//final GamerDialog2 gamerDialog = new GamerDialog2();
			//gamerDialog.pack();
			gamerDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Arbitre){
			ArbitreDialog arbitreDialog = new ArbitreDialog();
			//final GamerDialog2 gamerDialog = new GamerDialog2();
			//gamerDialog.pack();
			arbitreDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Medecin){
			MedecinDialog arbitreDialog = new MedecinDialog();
			//final GamerDialog2 gamerDialog = new GamerDialog2();
			//gamerDialog.pack();
			arbitreDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Visitor){
			VisitorDialog cDialog = new VisitorDialog();
			//final GamerDialog2 gamerDialog = new GamerDialog2();
			//gamerDialog.pack();
			cDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Match){
			MatchDialog mDialog = new MatchDialog();
			mDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Competition){
			CompetitionDialog mDialog = new CompetitionDialog();
			mDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Event){
			EventDialog mDialog = new EventDialog();
			mDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Session_Formation){
			FormationDialog mDialog = new FormationDialog();
			mDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Stade){
			StadeDialog mDialog = new StadeDialog();
			mDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Materiel){
			MaterielDialog mDialog = new MaterielDialog();
			mDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Autre){
			GrapheDialog gd = new GrapheDialog(admin,this);
			gd.setVisible(true);
		}
		if (ev.getSource() == btn_Club){
			ClubDialog mDialog = new ClubDialog();
			mDialog.setVisible(true);
		}
		
		
		
		
		
		if (ev.getSource() == btn_Exit){
			int choix = JOptionPane.showConfirmDialog(null,"Voulez Vous Vraiment Quitter ?","Quitter", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);if ( choix == JOptionPane.OK_OPTION  ){System.exit(1);}
		}

	}

}
