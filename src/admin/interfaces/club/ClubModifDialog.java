package admin.interfaces.club;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import dao.DAOClub;
import entities.Club;
import interfaces.BgBorder;
import interfaces.Login;

public class ClubModifDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	JTextField textField_nom;
	JTextField textField_ville;
	
	
	
	
	
	DAOClub daoc;
	Club c;
	
	
	
	
	ClubDialog parent;
	
	JButton btn_retour;
	JButton btn_appliquer;
	
	List<Club> listClubs = new ArrayList<Club>();
	
	public ClubModifDialog(Club comp, DAOClub daoc, ClubDialog par){
		
		this.daoc = daoc;
		this.c = comp;
		this.parent = par;
		
		
		
		this.setTitle("Club N� : "+c.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		// nom
		JLabel lbNom = new JLabel("Nom: ");
		lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
		lbNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lbNom.setForeground(new Color(238, 203, 51));
		lbNom.setBackground(Color.BLACK);
		lbNom.setBounds(99, 50, 200, 22);
	    panel.add(lbNom);
	    textField_nom = new JTextField();
	    textField_nom.setText(c.getNom());
	    textField_nom.setBounds(300, 50, 200, 29);
	    panel.add(textField_nom);
	   
	    
	   // 50/110/140/170/200
	    
	    
	   
		// date_debut
	    JLabel lblVille = new JLabel("Ville: ");
	    lblVille.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblVille.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblVille.setForeground(new Color(238, 203, 51));
	    lblVille.setBackground(Color.BLACK);
	    lblVille.setBounds(99, 82, 200, 22);
	    panel.add(lblVille);
	    
	    
	    
	    textField_ville = new JTextField();
	    textField_ville.setBounds(300, 82, 200, 29);
	    textField_ville.setText(c.getVille());
	    panel.add(textField_ville);
	    
	    
	 	
	 	//////
	    
	    
		
		
		
	 		//////
			btn_retour = new JButton("Retour");
			btn_retour.addActionListener(this);
			btn_retour.setForeground(Color.BLUE);
			btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_retour.setBounds(199, 353, 200, 29);
			panel.add(btn_retour);		
			//
			btn_appliquer = new JButton("Appliquer");
			btn_appliquer.addActionListener(this);
			btn_appliquer.setForeground(Color.BLUE);
			btn_appliquer.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_appliquer.setBounds(400, 353, 200, 29);
			panel.add(btn_appliquer);
			
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn_retour){dispose();}
		if(e.getSource() == btn_appliquer){
			
			
			
			
			
			
			Club m2 = new Club();
			
			m2.setId(c.getId());
			m2.setNom(textField_nom.getText());
			m2.setVille(textField_ville.getText());
			
			
			daoc.updateClub(m2);
			parent.dispose();
			ClubDialog mf = new ClubDialog();
			mf.setVisible(true);
			this.dispose();
			
		}
		
	}
	
	
	
	
}