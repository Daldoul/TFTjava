package admin.interfaces.club;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import entities.Club;
import interfaces.BgBorder;
import interfaces.Login;

public class ClubShowDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	
	JButton btn_retour;
	
	Club c ;
	
	public ClubShowDialog(Club cl) {
		this.c = cl;
		this.setTitle("Club N� "+c.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		
		
		JLabel lblNom = new JLabel("Nom: "+c.getNom());
	    lblNom.setHorizontalAlignment(SwingConstants.LEFT);
	    lblNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblNom.setForeground(new Color(238, 203, 51));
	    lblNom.setBackground(Color.BLACK);
	    lblNom.setBounds(50, 50, 400, 22);
	    panel.add(lblNom);
		//
	    JLabel lblDateDebut = new JLabel("Ville: "+c.getVille());
	    lblDateDebut.setHorizontalAlignment(SwingConstants.LEFT);
	    lblDateDebut.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblDateDebut.setForeground(new Color(238, 203, 51));
	    lblDateDebut.setBackground(Color.BLACK);
	    lblDateDebut.setBounds(50, 80, 400, 22);
	    panel.add(lblDateDebut);
	    //
	   
		
		
	    	//////
				btn_retour = new JButton("Retour");
				btn_retour.addActionListener(this);
				btn_retour.setForeground(Color.BLUE);
				btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
				btn_retour.setBounds(300, 353, 200, 29);
			    panel.add(btn_retour);		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn_retour) {this.dispose();}

	}

}
