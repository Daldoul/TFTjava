package admin.interfaces.formation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import entities.Formation;
import interfaces.BgBorder;
import interfaces.Login;

public class FormationShowDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	
JButton btn_retour;
	
	
	Formation f ;
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	
	public FormationShowDialog(Formation fo) {
		this.f = fo;
		this.setTitle("Session de Formation N� "+f.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		
		
		JLabel lblNom = new JLabel("Nom: "+f.getNom());
	    lblNom.setHorizontalAlignment(SwingConstants.LEFT);
	    lblNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblNom.setForeground(new Color(238, 203, 51));
	    lblNom.setBackground(Color.BLACK);
	    lblNom.setBounds(50, 50, 400, 22);
	    panel.add(lblNom);
		//
	    JLabel lblDateDebut = new JLabel("date debut: "+sdf.format(f.getDate_ouverture()));
	    lblDateDebut.setHorizontalAlignment(SwingConstants.LEFT);
	    lblDateDebut.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblDateDebut.setForeground(new Color(238, 203, 51));
	    lblDateDebut.setBackground(Color.BLACK);
	    lblDateDebut.setBounds(50, 80, 400, 22);
	    panel.add(lblDateDebut);
	    //
	    JLabel lblDateFin = new JLabel("date Fin: "+sdf.format(f.getDate_cloture()));
	    lblDateFin.setHorizontalAlignment(SwingConstants.LEFT);
	    lblDateFin.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblDateFin.setForeground(new Color(238, 203, 51));
	    lblDateFin.setBackground(Color.BLACK);
	    lblDateFin.setBounds(50, 110, 650, 22);
	    panel.add(lblDateFin);
	    //
	    JLabel lblLieux= new JLabel("Lieux: "+f.getLieux());
	    lblLieux.setHorizontalAlignment(SwingConstants.LEFT);
	    lblLieux.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblLieux.setForeground(new Color(238, 203, 51));
	    lblLieux.setBackground(Color.BLACK);
	    lblLieux.setBounds(50, 140, 650, 22);
	    panel.add(lblLieux);
	    //
	    JLabel lblType= new JLabel("Type: "+f.getType());
	    lblType.setHorizontalAlignment(SwingConstants.LEFT);
	    lblType.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblType.setForeground(new Color(238, 203, 51));
	    lblType.setBackground(Color.BLACK);
	    lblType.setBounds(50, 170, 400, 22);
	    panel.add(lblType);
	    //
	   
	   
		
		
		
		
	    	//////
				btn_retour = new JButton("Retour");
				btn_retour.addActionListener(this);
				btn_retour.setForeground(Color.BLUE);
				btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
				btn_retour.setBounds(300, 353, 200, 29);
			    panel.add(btn_retour);		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn_retour) {this.dispose();}

	}

}
