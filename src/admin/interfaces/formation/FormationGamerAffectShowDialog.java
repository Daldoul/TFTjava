package admin.interfaces.formation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.AbstractDocument;

import admin.interfaces.arbitre.ArbitreShowDialog;
import admin.interfaces.gamer.GamerShowDialog;
import dao.DAOForma_user;
import dao.DAOFormation;
import dao.DAOUtilisateur;
import entities.Forma_user;
import entities.Formation;
import entities.Utilisateur;
import filters.NumericAndLengthFilter;
import interfaces.BgBorder;
import interfaces.Login;

public class FormationGamerAffectShowDialog extends JDialog implements ActionListener, KeyListener, MouseListener {
	private static final long serialVersionUID = 1L;
	
	
	JButton btn_retour;
	JTextField txtclenum;
	JTextField txtclenom;
	JTextField txtcleprenom;
	
	JTextField txtclecin;
	JTextField txtcledresse;
	JTextField txtcletel;
	
	JTextField txtclegrade;
	JTextField txtcleemail;
	JTextField txtclerole;
	
	DAOForma_user daofu;
	List<Forma_user> listForma_user;
	
	DAOUtilisateur daou ;
	List<Utilisateur> listGamer;
	
	String col[] = { "Id", "Nom", "Prenom" , "CIN" , "Adresse" ,  "T�l�phone" , "Grade" , "E-mail", "Role","image"};
	JTable table ;
	DefaultTableModel model;
	TableRowSorter<TableModel> filtre;
	
	DAOFormation daof;
	Formation formation;
	FormationDialog parent;
	
	JMenuItem ouvrirItem;
    
    JMenuItem suppItem;
    NumericAndLengthFilter numericandlengthfilter = new NumericAndLengthFilter(11);
	
	@SuppressWarnings("serial")
	public FormationGamerAffectShowDialog(Formation forma, DAOFormation dao, FormationDialog par){
		this.formation = forma;
		this.daof = dao;
		this.parent=par;
		this.setTitle("Les Joueurs De La Session de Formation N�"+formation.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 1000, 700);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1000, 700);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis04.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		// remplissage
		daou = new DAOUtilisateur();
		daofu = new DAOForma_user();
		listGamer = new ArrayList<Utilisateur>();
		listForma_user = new ArrayList<Forma_user>();
		listForma_user= daofu.getForma_usersByFormaId(formation.getId());
		
		int l = listForma_user.size();
		for(int i = 0;i<l;i++ ){
			if( daou.getGamerById(listForma_user.get(i).getId_user()).getRoles()!= null ){
				if( daou.getGamerById(listForma_user.get(i).getId_user()).getRoles().equals("Joueur") ){
					listGamer.add(daou.getGamerById(listForma_user.get(i).getId_user()));
				}
			}
		}
		
		int t = listGamer.size();
	    Object data[][] = new Object[t][10];
	      for(int i = 0;i<t;i++ ){
	        data[i][0] = ""+listGamer.get(i).getId();
	        data[i][1] = ""+listGamer.get(i).getNom();
	        data[i][2] = ""+listGamer.get(i).getPrenom();
	        data[i][3] = ""+listGamer.get(i).getCin();
	        data[i][4] = ""+listGamer.get(i).getAdresse();
	        data[i][5] = ""+listGamer.get(i).getTelephone();
	        data[i][6] = ""+listGamer.get(i).getGrade();
	        data[i][7] = ""+listGamer.get(i).getEmail();
	        data[i][8] = ""+listGamer.get(i).getRoles();
	        
	        System.out.println("le champ image : "+listGamer.get(i).getImage()+".jpg");
	        
	        if(listGamer.get(i).getImage()!= null){try {
				data[i][9] = new ImageIcon(ImageIO.read(new File("C:/imagesUtilisateurs/"+listGamer.get(i).getCin()+".jpg")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}}
	        else {data[i][9] = "Pas l'image";}
	      }
        
        model = new DefaultTableModel(data, col){
	    	  /*
	    	  @SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int column) {
	    	        Class returnValue;
	    	        if ((column >= 0) && (column < getColumnCount())) {
	    	          returnValue = getValueAt(0, column).getClass();
	    	        } else {
	    	          returnValue = Object.class;
	    	        }
	    	        return returnValue;
	    	      }
	    	  */
	      };
	      table = new JTable(model){
	    	  	
	              @SuppressWarnings({ "unchecked", "rawtypes" })
				public Class getColumnClass(int column){return getValueAt(0, column).getClass();}
	              public boolean isCellEditable(int row, int column) {return false;}
	              
	      };

	      table.setRowHeight(80);
	      table.setCellSelectionEnabled(false);
	      table.setFocusable(false);
	      table.setRowSelectionAllowed(true);
//	      table.setWrapStyleWord(true);
//        table.setLineWrap(true);
	      table.setShowGrid(true);
	      table.setShowVerticalLines(true);
	      
	      
	      filtre = new TableRowSorter<TableModel>(model);
	      
	      
	      table.setRowSorter(filtre);
	      
	      table.addMouseListener(this);
	      //table.addAncestorListener(this);
	      
	      JScrollPane panJtab = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	      //panJtab.setPreferredSize(new Dimension(1240, 550));
	      panJtab.setBounds(15, 50, 970, 570);
	      
	      panel.add(panJtab); 
	      
		    
		// les recherches
		
	    txtclenum = new JTextField();
	    txtclenum.addKeyListener(this);
	    ( (AbstractDocument) txtclenum.getDocument()).setDocumentFilter(numericandlengthfilter);
	    txtclenum.setBounds(15, 21, 96, 29);
		panel.add(txtclenum);
		txtclenum.setColumns(11);
		
	  	txtclenom = new JTextField();
	  	txtclenom.addKeyListener(this);
	  	txtclenom.setBounds(111, 21, 95, 29);
		panel.add(txtclenom);
		txtclenom.setColumns(20);
	  	
	  	txtcleprenom = new JTextField();
	  	txtcleprenom.addKeyListener(this);
	  	txtcleprenom.setBounds(206, 21, 95, 29);
		panel.add(txtcleprenom);
		txtcleprenom.setColumns(20);
	  	
	  	txtclecin = new JTextField();
	  	txtclecin.addKeyListener(this);
	  	( (AbstractDocument) txtclecin.getDocument()).setDocumentFilter(numericandlengthfilter);
	  	txtclecin.setBounds(301, 21, 94, 29);
		panel.add(txtclecin);
		txtclecin.setColumns(11);
	  	
		txtcledresse = new JTextField();
		txtcledresse.addKeyListener(this);
		txtcledresse.setBounds(395, 21, 95, 29);
		panel.add(txtcledresse);
		txtcledresse.setColumns(20);
	  	
	  	
	  	txtcletel = new JTextField();
	  	txtcletel.addKeyListener(this);
	  	( (AbstractDocument) txtcletel.getDocument()).setDocumentFilter(numericandlengthfilter);
	  	txtcletel.setBounds(490, 21, 95, 29);
		panel.add(txtcletel);
		txtcletel.setColumns(11);
	  	
	  	txtclegrade = new JTextField();
	  	txtclegrade.addKeyListener(this);
	  	txtclegrade.setBounds(585, 21, 95, 29);
		panel.add(txtclegrade);
		txtclegrade.setColumns(20);
	  	
	  	txtcleemail = new JTextField();
	  	txtcleemail.addKeyListener(this);
	  	txtcleemail.setBounds(680, 21, 95, 29);
		panel.add(txtcleemail);
		txtcleemail.setColumns(20);
	  	
	  	txtclerole = new JTextField();
	  	txtclerole.addKeyListener(this);
	  	txtclerole.setBounds(775, 21, 95, 29);
		panel.add(txtclerole);
		txtclerole.setColumns(20);
		/////
		
		
		
		
		//////
		btn_retour = new JButton("Retour");
		btn_retour.addActionListener(this);
		btn_retour.setForeground(Color.BLUE);
		btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_retour.setBounds(750, 630, 200, 29);
	    panel.add(btn_retour);
		
		
	 // Create popup menu, attach popup menu listener
	    JPopupMenu popupMenu = new JPopupMenu("Menu");
	    
	    ouvrirItem = new JMenuItem("Ouvrir");ouvrirItem.addActionListener(this);
	    popupMenu.add(ouvrirItem);
	   
	    
	    popupMenu.addSeparator();
	    suppItem = new JMenuItem("D�sinscrir");suppItem.addActionListener(this);
	    popupMenu.add(suppItem);
	    
	    
	    table.setComponentPopupMenu(popupMenu);
		
		
	    
	    
	    
	    
		
	}
	public void rebuild(){
		SwingUtilities.updateComponentTreeUI(this);
		model.fireTableDataChanged();
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e1) {
		if (e1.getSource() == btn_retour) {dispose();}
		
		if (e1.getSource() == ouvrirItem) {
            int row = table.getSelectedRow();
            //int column = table.getSelectedColumn();
            
            Utilisateur u = listGamer.get(row);
            
            // you can play more here to get that cell value and all
            new GamerShowDialog(u).setVisible(true);
            //this.dispose();
            
        }
		
		if (e1.getSource() == suppItem) {
            int row = table.getSelectedRow();
            //int column = table.getSelectedColumn();
            
            Utilisateur u = listGamer.get(row);
            
         // you can play more here to get that cell value and all
            FormationGamerAffectSuppDialog fsd=  new FormationGamerAffectSuppDialog(formation,daof,u,daofu,parent,this);
            fsd.setVisible(true);
            //this.dispose();
            
            //System.out.println("cccccccc");
        } 
		 
		
		
		

	}


	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mousePressed(MouseEvent e2) {
		if(e2.getSource()==table){
			
			if (e2.getClickCount() == 2) {
	            JTable target = (JTable) e2.getSource();
	            int row = target.getSelectedRow();
	            //int column = target.getSelectedColumn();
	            Utilisateur u = listGamer.get(row);
	            // you can play more here to get that cell value and all
	            new ArbitreShowDialog(u).setVisible(true);
	            //this.dispose();
	           
	            //System.out.println("cccccccc");
	        }
			
			
			if (e2.getButton()== MouseEvent.BUTTON3){
				table.setRowSelectionAllowed(true);
				table.setColumnSelectionAllowed(false);
				table.changeSelection(e2.getY()/table.getRowHeight(),0,false, false); 
			} 
		}
		
	}


	@Override
	public void mouseReleased(MouseEvent e) {
		
		
	}


	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void keyReleased(KeyEvent e) {
		//numero
		if(e.getSource()==txtclenum){
			String text = txtclenum.getText();
	        if (text.length() == 0) {
	          filtre.setRowFilter(null);
	        } else {
	          filtre.setRowFilter(RowFilter.regexFilter(text,0)   );
	        }
			
		}
		////nom
		if(e.getSource()==txtclenom){
			String text = txtclenom.getText();
	        if (text.length() == 0) {
	          filtre.setRowFilter(null);
	        } else {
	          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,1)   );
	        }
			
		}
		////prenom
		if(e.getSource()==txtcleprenom){
			String text = txtcleprenom.getText();
	        if (text.length() == 0) {
	          filtre.setRowFilter(null);
	        } else {
	          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,2)   );
	        }
			
		}
		/////cin
		if(e.getSource()==txtclecin){
			String text = txtclecin.getText();
	        if (text.length() == 0) {
	          filtre.setRowFilter(null);
	        } else {
	          filtre.setRowFilter(RowFilter.regexFilter(text,3)   );
	        }
			
		}
		//adresse
		if(e.getSource()==txtcledresse){
			String text = txtcledresse.getText();
	        if (text.length() == 0) {
	          filtre.setRowFilter(null);
	        } else {
	          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,4)   );
	        }
			
		}
		//telephone
		if(e.getSource()==txtcletel){
			String text = txtcletel.getText();
	        if (text.length() == 0) {
	          filtre.setRowFilter(null);
	        } else {
	          filtre.setRowFilter(RowFilter.regexFilter(text,5)   );
	        }
			
		}
		//grade		
		if(e.getSource()==txtclegrade){
			String text = txtclegrade.getText();
	        if (text.length() == 0) {
	          filtre.setRowFilter(null);
	        } else {
	          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,6)   );
	        }
			
		}
		//email		
				if(e.getSource()==txtcleemail){
					String text = txtcleemail.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,7)   );
			        }
					
				}
				//role		
				if(e.getSource()==txtclerole){
					String text = txtclerole.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,8)   );
			        }
					
				}		
	}
	
	    
	
}
