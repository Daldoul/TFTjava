package admin.interfaces.graph;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import dao.DAOClub;
import dao.DAOUtilisateur;
import entities.Club;
import entities.Utilisateur;
import interfaces.AdminFrame;
import interfaces.BgBorder;
import interfaces.Login;

public class ClubGraphDiaog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	JButton btn_retour;
	JButton btn_new;
	
	DAOClub daoc = new DAOClub();
	List<Club> listclubs = new ArrayList<Club>();
	
	DAOUtilisateur dao = new DAOUtilisateur();
	Utilisateur admin;
	AdminFrame parent;
	JPanel panel;
	JLabel imagelabel;
	ImageIcon img;
	public ClubGraphDiaog(Utilisateur ad, AdminFrame par){
		this.admin=ad;
		this.parent=par;
		this.setTitle("TFT - Administration");
		this.setResizable(false);
		this.setBounds(100, 100, 1000, 600);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		panel = new JPanel();
		panel.setBounds(0, 0, 1000, 700);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis04.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		
		/////
		
		
		img = null;
		try {
			img = new ImageIcon(ImageIO.read(new File("C:/Graphes/Clubs_Chart.jpeg")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    if(img!=null){
	    	imagelabel = new JLabel(img);
	    	imagelabel.setBounds(225, 75, 560, 370);
		    panel.add(imagelabel);
	    }
	    else{
	    	imagelabel = new JLabel("Pas d'image - cliquer sur Actualiser");
		    imagelabel.setBounds(225, 75, 560, 370);
		    panel.add(imagelabel);
	    }
		
		
		
		
		
		
		
		
		btn_retour = new JButton("Retour");
		btn_retour.addActionListener(this);
		btn_retour.setForeground(Color.BLUE);
		btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_retour.setBounds(750, 520, 200, 29);
	    panel.add(btn_retour);
	
	    
	    //////
		btn_new = new JButton("Actualiser");
		btn_new.addActionListener(this);
		btn_new.setForeground(Color.BLUE);
		btn_new.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_new.setBounds(549, 520, 200, 29);
		panel.add(btn_new);	
	}

	@Override
	public void actionPerformed(ActionEvent e1) {
		if (e1.getSource() == btn_retour) {dispose();}
		
		if (e1.getSource() == btn_new) {
			//String mobilebrands[] = {"Administrateur", "Arbitre", "Joueur", "Medecin","Respensable","Visiteurs"};
			DefaultPieDataset dataset = new DefaultPieDataset( );
			
			listclubs = daoc.getClubs();
			int t = listclubs.size();
			for(int i = 0; i<t; i++){
				dataset.setValue(listclubs.get(i).getNom() ,dao.getNbJoueurByClubId(listclubs.get(i).getId()));
			}
			
			
			
			
			JFreeChart chart = ChartFactory.createPieChart("Les Clubs", dataset, true, true, false );
			
			int width = 560; /* Width of the image */
		    int height = 370; /* Height of the image */ 
		    File pieChart = new File( "C:/Graphes/Clubs_Chart.jpeg" );
		    try {
				ChartUtilities.saveChartAsJPEG( pieChart , chart , width , height );
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    panel.remove(imagelabel);
		    SwingUtilities.updateComponentTreeUI(panel);
		    img = null;
			try {
				img = new ImageIcon(ImageIO.read(new File("C:/Graphes/Clubs_Chart.jpeg")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    if(img!=null){
		    	imagelabel = new JLabel(img);
		    	imagelabel.setBounds(225, 75, 560, 370);
			    panel.add(imagelabel);
		    }
		    else{
		    	imagelabel = new JLabel("Pas d'image - cliquer sur Actualiser");
			    imagelabel.setBounds(225, 75, 560, 370);
			    panel.add(imagelabel);
		    }
		    SwingUtilities.updateComponentTreeUI(panel);
			
			
        }

	}

}
