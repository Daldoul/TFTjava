package admin.interfaces.graph;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import entities.Utilisateur;
import interfaces.AdminFrame;
import interfaces.BgBorder;
import interfaces.Login;

public class GrapheDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	JButton btn_Graph_User;
	JButton btn_Clubs;
	
	JButton btn_Retour;
	
	Utilisateur admin;
	AdminFrame parent;
	
	public GrapheDialog(Utilisateur ad, AdminFrame par){
		this.parent=par;
		this.admin=ad;
		this.setTitle("TFT - Administration");
		this.setResizable(false);
		this.setBounds(100, 100, 1000, 600);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1000, 600);
		this.getContentPane().add(panel);
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis003.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		
		JPanel panel_1 = new JPanel();
	     panel_1.setBorder(new LineBorder(new Color(204, 255, 255), 4));
	     panel_1.setBackground(new Color(0, 102, 153));
	     panel_1.setBounds(200, 60, 600, 70);
	     panel.add(panel_1);
	     panel_1.setLayout(null);
	     
	     JLabel lblInscri = new JLabel("Les Graphes");
	     lblInscri.setHorizontalAlignment(SwingConstants.CENTER);
	     lblInscri.setForeground(Color.WHITE);
	     lblInscri.setFont(new Font("Lucida Blackletter", Font.PLAIN, 26));
	     lblInscri.setBounds(6, 6, 590, 58);
	     panel_1.add(lblInscri);
		
		btn_Graph_User = new JButton("Les Utilisateurs");
		btn_Graph_User.addActionListener(this);
		btn_Graph_User.setForeground(Color.BLUE);
		btn_Graph_User.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_Graph_User.setBounds(299, 271, 200, 29);
	    panel.add(btn_Graph_User);
	     
	    btn_Clubs = new JButton("Les Clubs");
	    btn_Clubs.addActionListener(this);
	    btn_Clubs.setForeground(Color.BLUE);
	    btn_Clubs.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Clubs.setBounds(500, 271, 200, 29);
	    panel.add(btn_Clubs);
	    

	    btn_Retour = new JButton("Quitter");
	    btn_Retour.addActionListener(this);
	    btn_Retour.setForeground(Color.BLUE);
	    btn_Retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_Retour.setBounds(750, 519, 200, 29);
	    panel.add(btn_Retour);
	   
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		if (ev.getSource() == btn_Graph_User){
			
			new UtilisateurGraphDiaog( admin,  parent).setVisible(true);
			
		}
		
		if (ev.getSource() == btn_Clubs){
			
			new ClubGraphDiaog( admin,  parent).setVisible(true);
			
		}
		
		if (ev.getSource() == btn_Retour){
			this.dispose();
		}
		
	}

}
