package admin.interfaces.stade;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOMatch;
import dao.DAOStade;
import entities.Match;
import entities.Stade;
import interfaces.BgBorder;
import interfaces.Login;

public class StadeSuppDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	JButton btn_retour;
	JButton btn_supp;
	Stade s;
	DAOStade DAO;
	StadeDialog parent;
	
	DAOMatch daom = new DAOMatch();
	List<Match> listMatch = new ArrayList<Match>();
	
	
	public StadeSuppDialog(Stade st, DAOStade daos, StadeDialog par){
		
		DAO = daos;
		s = st;
		parent = par;
		
		this.setTitle("Supprimer Stade N� "+s.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 400, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 400, 200);
		this.getContentPane().add(panel);
		
		listMatch = daom.getMatchsByLieuxId(s.getId());
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis06.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		
		
		JLabel msg = new JLabel("Voulez vous Supprimer ");
		msg.setHorizontalAlignment(SwingConstants.LEFT);
		msg.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		msg.setForeground(Color.BLACK);
		msg.setBackground(Color.BLACK);
		msg.setBounds(120, 40, 250, 30);
	    panel.add(msg);
	    
	    JLabel name = new JLabel(""+s.getNom()+" ?");
	    name.setHorizontalAlignment(SwingConstants.LEFT);
	    name.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
	    name.setForeground(Color.BLACK);
	    name.setBackground(Color.BLACK);
	    name.setBounds(120, 70, 250, 30);
	    panel.add(name);
		
		
		
		
		btn_retour = new JButton("Non");
		btn_retour.addActionListener(this);
		btn_retour.setForeground(Color.BLUE);
		btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_retour.setBounds(99, 130, 100, 29);
	    panel.add(btn_retour);
	    
	    btn_supp = new JButton("Oui");
	    btn_supp.addActionListener(this);
	    btn_supp.setForeground(Color.BLUE);
	    btn_supp.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_supp.setBounds(200, 130, 100, 29);
	    panel.add(btn_supp);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()== btn_retour){dispose();}
		
		if(e.getSource() == btn_supp){
			parent.dispose();
			
			
			
			DAO.deleteStade(s,listMatch);
			StadeDialog mat = new StadeDialog();
			mat.setVisible(true);
			//parent.panel.revalidate();
			this.dispose();
		}

	}

}