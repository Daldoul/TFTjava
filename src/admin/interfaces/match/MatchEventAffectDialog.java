package admin.interfaces.match;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOEvenement;
import dao.DAOMatch;
import entities.Evenement;
import entities.Match;
import interfaces.BgBorder;
import interfaces.Login;

public class MatchEventAffectDialog extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	JButton btn_retour;
	JButton btn_affect;
	Match m;
	DAOMatch DAO;
	
	Evenement event;
	DAOEvenement daoe;
	List<Evenement> listEvenement=new ArrayList<Evenement>();
	
	MatchDialog parent;
	
	JComboBox<String> select_event_name;
	
	public MatchEventAffectDialog(Match match, DAOMatch daom, MatchDialog par){
		
		DAO = daom;
		m = match;
		parent = par;
		
		this.setTitle("Affecter Match N� "+m.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 400, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 400, 200);
		this.getContentPane().add(panel);
		
		
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis06.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		
		// lieux
		JLabel lblevent = new JLabel("Evenement: ");
		lblevent.setHorizontalAlignment(SwingConstants.RIGHT);
		lblevent.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lblevent.setForeground(Color.BLACK);
		lblevent.setBackground(Color.BLACK);
		lblevent.setBounds(10, 50, 130, 22);
	    panel.add(lblevent);
		
		daoe = new DAOEvenement();
		listEvenement= daoe.getEvenements();
		int t = listEvenement.size();
		String event_name[] = new String[t];
		      for(int i = 0;i<t;i++ ){
		    	  event_name[i]=listEvenement.get(i).getNom();
		      }
		select_event_name = new JComboBox<String>(event_name);
		select_event_name.setBounds(150, 45, 200, 29);
		panel.add(select_event_name);
		
		event = daoe.getEventById(listEvenement.get(select_event_name.getSelectedIndex()).getId());
	    
		
		
		
		
		btn_retour = new JButton("Retour");
		btn_retour.addActionListener(this);
		btn_retour.setForeground(Color.BLUE);
		btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_retour.setBounds(99, 130, 100, 29);
	    panel.add(btn_retour);
	    
	    btn_affect = new JButton("Affecter");
	    btn_affect.addActionListener(this);
	    btn_affect.setForeground(Color.BLUE);
	    btn_affect.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_affect.setBounds(200, 130, 100, 29);
	    panel.add(btn_affect);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()== btn_retour){dispose();}
		
		if(e.getSource() == btn_affect){
			event = daoe.getEventById(listEvenement.get(select_event_name.getSelectedIndex()).getId());
			parent.dispose();
			DAO.affectMatchEvent(m,event);
			MatchDialog mat = new MatchDialog();
			mat.setVisible(true);
			this.dispose();
		}

	}

	

}
