package admin.interfaces.match;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOCompetition;
import dao.DAOMatch;
import entities.Competition;
import entities.Match;
import interfaces.BgBorder;
import interfaces.Login;

public class MatchCompAffectDialog extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;
	
	JButton btn_retour;
	JButton btn_affect;
	Match m;
	DAOMatch DAO;
	
	Competition competition;
	DAOCompetition daoc;
	List<Competition> listCompetition=new ArrayList<Competition>();
	
	MatchDialog parent;
	
	JComboBox<String> select_comp_name;
	
	public MatchCompAffectDialog(Match match, DAOMatch daom, MatchDialog par){
		
		DAO = daom;
		m = match;
		parent = par;
		
		this.setTitle("Affecter Match N� "+m.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 400, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 400, 200);
		this.getContentPane().add(panel);
		
		
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis06.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		
		// lieux
		JLabel lblevent = new JLabel("Competition: ");
		lblevent.setHorizontalAlignment(SwingConstants.RIGHT);
		lblevent.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lblevent.setForeground(Color.BLACK);
		lblevent.setBackground(Color.BLACK);
		lblevent.setBounds(10, 50, 150, 22);
	    panel.add(lblevent);
	    competition = new Competition();
		daoc = new DAOCompetition();
		listCompetition= daoc.getCompetitions();
		int t = listCompetition.size();
		String comp_name[] = new String[t];
		      for(int i = 0;i<t;i++ ){
		    	  comp_name[i]=listCompetition.get(i).getNom();
		      }
		select_comp_name = new JComboBox<String>(comp_name);
		select_comp_name.setBounds(170, 45, 200, 29);
		panel.add(select_comp_name);
		
	    
	    
		
		
		
		
		btn_retour = new JButton("Retour");
		btn_retour.addActionListener(this);
		btn_retour.setForeground(Color.BLUE);
		btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_retour.setBounds(99, 130, 100, 29);
	    panel.add(btn_retour);
	    
	    btn_affect = new JButton("Affecter");
	    btn_affect.addActionListener(this);
	    btn_affect.setForeground(Color.BLUE);
	    btn_affect.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_affect.setBounds(200, 130, 100, 29);
	    panel.add(btn_affect);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()== btn_retour){dispose();}
		
		if(e.getSource() == btn_affect){
			competition = daoc.getCompById(listCompetition.get(select_comp_name.getSelectedIndex()).getId());
			parent.dispose();
			DAO.affectMatchComp(m,competition);
			MatchDialog mat = new MatchDialog();
			mat.setVisible(true);
			this.dispose();
		}

	}

}
