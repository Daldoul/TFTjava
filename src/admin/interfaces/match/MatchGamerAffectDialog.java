package admin.interfaces.match;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOMatch_user;
import dao.DAOUtilisateur;
import entities.Match;
import entities.Utilisateur;
import interfaces.BgBorder;
import interfaces.Login;
import mail.REmailUser;

public class MatchGamerAffectDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	JButton btn_retour;
	JButton btn_affect;
	Match m;
	
	DAOMatch_user DAO;
	Utilisateur gamer;
	DAOUtilisateur daou;
	List<Utilisateur> listGamer=new ArrayList<Utilisateur>();
	
	MatchDialog parent;
	
	JComboBox<String> select_gamer_name;
	
	public MatchGamerAffectDialog(Match match, MatchDialog par){
		
		DAO = new DAOMatch_user();
		daou = new DAOUtilisateur();
		m = match;
		parent = par;
		
		this.setTitle("Affecter au Match N� "+m.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 400, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 400, 200);
		this.getContentPane().add(panel);
		
		/////
		btn_affect = new JButton("Affecter");
		/////
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis06.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		
		// Gamer
		JLabel lblGamer = new JLabel("Joueur: ");
		lblGamer.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGamer.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lblGamer.setForeground(Color.BLACK);
		lblGamer.setBackground(Color.BLACK);
		lblGamer.setBounds(10, 50, 130, 22);
	    panel.add(lblGamer);
		
	    daou = new DAOUtilisateur();
		listGamer= daou.getGamersByGrade(m.getType());
		int t = listGamer.size();
		if(t>0){
			String gamer_name[] = new String[t];
		      for(int i = 0;i<t;i++ ){
		    	  gamer_name[i]=listGamer.get(i).getNom()+" "+listGamer.get(i).getPrenom();
		      }
		
		  
		select_gamer_name = new JComboBox<String>(gamer_name);
		}else{
			String gamer_name[] = new String[1];
			gamer_name[0] = "Aucun Joueur de grade compatible";
			select_gamer_name = new JComboBox<String>(gamer_name);
			btn_affect.setEnabled(false);
			
		}
		select_gamer_name.setBounds(150, 45, 200, 29);
		panel.add(select_gamer_name);
		
		
	    
		
		
		
		
		btn_retour = new JButton("Retour");
		btn_retour.addActionListener(this);
		btn_retour.setForeground(Color.BLUE);
		btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_retour.setBounds(99, 130, 100, 29);
	    panel.add(btn_retour);
	    
	    
	    btn_affect.addActionListener(this);
	    btn_affect.setForeground(Color.BLUE);
	    btn_affect.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_affect.setBounds(200, 130, 100, 29);
	    panel.add(btn_affect);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()== btn_retour){dispose();}
		
		if(e.getSource() == btn_affect){
			gamer = daou.getGamerById(listGamer.get(select_gamer_name.getSelectedIndex()).getId());
			//parent.dispose();
			DAO.affectMatchGamer(m,gamer);
			REmailUser mail = new REmailUser();
			mail.REmailUserMatch(gamer, m);
			//MatchDialog mat = new MatchDialog();
			//mat.setVisible(true);
			this.dispose();
			
		}

	}

	public void sendMailToGamer(Utilisateur gamer, Match match){
		System.out.println("------- Je doit envoyer un email");
	}

}
