package admin.interfaces.match;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOCompetition;
import dao.DAOEvenement;
import dao.DAOStade;
import entities.Competition;
import entities.Evenement;
import entities.Match;
import entities.Stade;
import interfaces.BgBorder;
import interfaces.Login;

public class MatchShowDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	JButton btn_retour;
	
	DAOStade daos ;
	Stade s ;
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	public MatchShowDialog(Match m){
		this.setTitle("Match N° "+m.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		daos = new DAOStade();
		s = new Stade();
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		s = daos.getStadeById(Integer.parseInt(m.getLieux()));
		
		JLabel lblNom = new JLabel("Lieux: "+s.getNom());
	    lblNom.setHorizontalAlignment(SwingConstants.LEFT);
	    lblNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblNom.setForeground(new Color(238, 203, 51));
	    lblNom.setBackground(Color.BLACK);
	    lblNom.setBounds(50, 50, 400, 22);
	    panel.add(lblNom);
		//
	    JLabel lblPrenom = new JLabel("Type: "+m.getType());
	    lblPrenom.setHorizontalAlignment(SwingConstants.LEFT);
	    lblPrenom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblPrenom.setForeground(new Color(238, 203, 51));
	    lblPrenom.setBackground(Color.BLACK);
	    lblPrenom.setBounds(50, 80, 400, 22);
	    panel.add(lblPrenom);
	    //
	    JLabel lblCin = new JLabel("date Debut: "+sdf.format(m.getDate_debut()));
	    lblCin.setHorizontalAlignment(SwingConstants.LEFT);
	    lblCin.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblCin.setForeground(new Color(238, 203, 51));
	    lblCin.setBackground(Color.BLACK);
	    lblCin.setBounds(50, 110, 650, 22);
	    panel.add(lblCin);
	    //
	    JLabel lblAdresse = new JLabel("Date fin: "+sdf.format(m.getDate_fin()));
	    lblAdresse.setHorizontalAlignment(SwingConstants.LEFT);
	    lblAdresse.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblAdresse.setForeground(new Color(238, 203, 51));
	    lblAdresse.setBackground(Color.BLACK);
	    lblAdresse.setBounds(50, 140, 650, 22);
	    panel.add(lblAdresse);
	    //
	    
	    
	    JLabel lblEvent;
	    if(m.getId_competition()!=0){
	    DAOEvenement daoe = new DAOEvenement();
	    Evenement e = daoe.getEventById(m.getId_competition());
	    lblEvent = new JLabel("Evenement: "+e.getNom());
	    }else{
	    	lblEvent = new JLabel("Evenement: Libre");
	    }	    
	    lblEvent.setHorizontalAlignment(SwingConstants.LEFT);
	    lblEvent.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblEvent.setForeground(new Color(238, 203, 51));
	    lblEvent.setBackground(Color.BLACK);
	    lblEvent.setBounds(50, 170, 400, 22);
	    panel.add(lblEvent);
	    //
	    JLabel lblGrade;
	    if(m.getId_competition()!=0){
	    DAOCompetition daoc = new DAOCompetition();
	    Competition c = daoc.getCompById(m.getId_competition());
	    lblGrade = new JLabel("Comptétition: "+c.getNom());
	    }else{
	    	lblGrade = new JLabel("Comptétition: Libre");
	    }
	    lblGrade.setHorizontalAlignment(SwingConstants.LEFT);
	    lblGrade.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblGrade.setForeground(new Color(238, 203, 51));
	    lblGrade.setBackground(Color.BLACK);
	    lblGrade.setBounds(50, 200, 400, 22);
	    panel.add(lblGrade);
	   
		
		
		
		
	    	//////
				btn_retour = new JButton("Retour");
				btn_retour.addActionListener(this);
				btn_retour.setForeground(Color.BLUE);
				btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
				btn_retour.setBounds(300, 353, 200, 29);
			    panel.add(btn_retour);		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn_retour) {this.dispose();}

	}

}
