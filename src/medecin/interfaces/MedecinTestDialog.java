package medecin.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.AbstractDocument;

import dao.DAOTest;
import dao.DAOTest_user;
import dao.DAOUtilisateur;
import entities.Test;
import entities.Test_user;
import entities.Utilisateur;
import filters.NumericAndLengthFilter;
import interfaces.BgBorder;
import interfaces.Login;
import outils.FileChooser3;

public class MedecinTestDialog extends JDialog implements ActionListener, KeyListener, MouseListener {
	private static final long serialVersionUID = 1L;
	
	

	JButton btn_retour;
	
	
	JTextField txtclenum;
	JTextField txtcledate;
	JTextField txtcletype;
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	JMenuItem ouvrirItem;
	JMenuItem gamerItem;
	JMenuItem gamerAddItem;
	JMenuItem depotItem;
    
    NumericAndLengthFilter numericandlengthfilter = new NumericAndLengthFilter(11);
	
	///////////////////
	
    File f1;
	File dir;
	
	public static final int TAILLE_TAMPON = 10240; // 10 ko
	
	String chemin = "C:/rapportUtilisateurs/";
	ImageIcon iconExc = new ImageIcon(getClass().getResource("/images/Exception.png"));
	ImageIcon iconWar = new ImageIcon(getClass().getResource("/images/ERROR35.png"));
	ImageIcon iconSucc = new ImageIcon(getClass().getResource("/images/Succes.png"));
	
	///////////////////////////////
	DAOTest daot = new DAOTest();
	List<Test> listtests = new ArrayList<Test>();
	
	String col[] = { "Id", "Date", "Type"};
	JTable table ;
	DefaultTableModel model;
	TableRowSorter<TableModel> filtre;
	
	Utilisateur medecin;
	DAOUtilisateur daom;
	MedecinFrame parent;
	////////////////////////////
	@SuppressWarnings("serial")
	public MedecinTestDialog(Utilisateur med, DAOUtilisateur dao, MedecinFrame par){
		
		this.medecin = med;
		this.daom = dao;
		this.parent = par;
		
		this.setTitle("Vos Testes");
		this.setResizable(false);
		this.setBounds(100, 100, 1000, 700);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1000, 700);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis04.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		
		/////
		
		
		
		
		DAOTest_user daotu = new DAOTest_user();
		List<Test_user> listtest_user = new ArrayList<Test_user>();
		listtest_user = daotu.getTest_usersByMedecinId(medecin.getId());
		
		int t1 = listtest_user.size();
		for(int i = 0;i<t1;i++ ){
			listtests.add(daot.getTestById(listtest_user.get(i).getId_test()));
		}
		
		
		int t = listtests.size();
		
	    Object data[][] = new Object[t][3];
	      for(int i = 0;i<t;i++ ){
	    	
	    	data[i][0] = ""+listtests.get(i).getId();
	        data[i][1] = ""+sdf.format(listtests.get(i).getDate());
	        data[i][2] = ""+listtests.get(i).getType();
	        
	        
	      }
        
        model = new DefaultTableModel(data, col){
	    	  /*
	    	  @SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int column) {
	    	        Class returnValue;
	    	        if ((column >= 0) && (column < getColumnCount())) {
	    	          returnValue = getValueAt(0, column).getClass();
	    	        } else {
	    	          returnValue = Object.class;
	    	        }
	    	        return returnValue;
	    	      }
	    	  */
	      };
	      table = new JTable(model){
	    	  	
	              @SuppressWarnings({ "unchecked", "rawtypes" })
				public Class getColumnClass(int column){return getValueAt(0, column).getClass();}
	              public boolean isCellEditable(int row, int column) {return false;}
	              
	      };

	      table.setRowHeight(80);
	      table.setCellSelectionEnabled(false);
	      table.setFocusable(false);
	      table.setRowSelectionAllowed(true);
//	      table.setWrapStyleWord(true);
//        table.setLineWrap(true);
	      table.setShowGrid(true);
	      table.setShowVerticalLines(true);
	      
	      
	      filtre = new TableRowSorter<TableModel>(model);
	      
	      
	      table.setRowSorter(filtre);
	      
	      table.addMouseListener(this);
	      //table.addAncestorListener(this);
	      
	      JScrollPane panJtab = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	      //panJtab.setPreferredSize(new Dimension(1240, 550));
	      panJtab.setBounds(15, 50, 970, 570);
	      
	      panel.add(panJtab); 
	      
	      
			// les recherches
			
		    txtclenum = new JTextField();
		    txtclenum.addKeyListener(this);
		    ( (AbstractDocument) txtclenum.getDocument()).setDocumentFilter(numericandlengthfilter);
		    txtclenum.setBounds(15, 21, 96, 29);
			panel.add(txtclenum);
			txtclenum.setColumns(11);
			
		  	txtcledate = new JTextField();
		  	txtcledate.addKeyListener(this);
		  	( (AbstractDocument) txtcledate.getDocument()).setDocumentFilter(numericandlengthfilter);
		  	txtcledate.setBounds(111, 21, 95, 29);
			panel.add(txtcledate);
			txtcledate.setColumns(20);
		  	
		  	txtcletype = new JTextField();
		  	txtcletype.addKeyListener(this);
		  	txtcletype.setBounds(206, 21, 95, 29);
			panel.add(txtcletype);
			txtcletype.setColumns(20);
		  	
			/////
			
			
			
			
			//////
			btn_retour = new JButton("Retour");
			btn_retour.addActionListener(this);
			btn_retour.setForeground(Color.BLUE);
			btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_retour.setBounds(750, 630, 200, 29);
		    panel.add(btn_retour);
		
		    
			
			
		 // Create popup menu, attach popup menu listener
		    JPopupMenu popupMenu = new JPopupMenu("Menu");
		    
		    ouvrirItem = new JMenuItem("Ouvrir");ouvrirItem.addActionListener(this);
		    popupMenu.add(ouvrirItem);
		    popupMenu.addSeparator();
		    gamerItem = new JMenuItem("Les Joueurs");gamerItem.addActionListener(this);
		    popupMenu.add(gamerItem);
		    gamerAddItem = new JMenuItem("Ajouter Joueur");gamerAddItem.addActionListener(this);
		    popupMenu.add(gamerAddItem);
		    popupMenu.addSeparator();
		    depotItem = new JMenuItem("Deposer Rapport");depotItem.addActionListener(this);
		    popupMenu.add(depotItem);
		   
		    
		    
		    table.setComponentPopupMenu(popupMenu);
		
		
		
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e1) {
		if (e1.getSource() == btn_retour) {dispose();}
		
		if (e1.getSource() == ouvrirItem) {
            int row = table.getSelectedRow();
            //int column = table.getSelectedColumn();
            Test t = listtests.get(row);
            // you can play more here to get that cell value and all
            new TestShowDialog(t).setVisible(true);
            //this.dispose();
            
        }
		if (e1.getSource() == gamerItem) {
            int row = table.getSelectedRow();
            //int column = table.getSelectedColumn();
            Test t = listtests.get(row);
            // you can play more here to get that cell value and all
            new GamerTestShowDialog(t,medecin,daom,parent).setVisible(true);
            //this.dispose();
            
        }
		
		if (e1.getSource() == gamerAddItem) {
            int row = table.getSelectedRow();
            //int column = table.getSelectedColumn();
            Test t = listtests.get(row);
            // you can play more here to get that cell value and all
            new GamerTestAddDialog(t,medecin,daom,parent).setVisible(true);
            //this.dispose();
            
        }
		if (e1.getSource() == depotItem) {
            int row = table.getSelectedRow();
            Test t = listtests.get(row);
            
            
            FileChooser3 fc = new FileChooser3(this);
			if(fc.getSelectedFile()!=null){
			f1=fc.getSelectedFile();
			
			dir=fc.getCurrentDirectory();
			

				try {
					ecrireFichier(f1, medecin.getCin()+"-"+t.getId()+".pdf", chemin);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null,"Erreur..", "erreur fichier image ",JOptionPane.INFORMATION_MESSAGE, iconSucc);
					e.printStackTrace();}
			
				
				MedecinTestDialog mtd = new MedecinTestDialog(medecin,daom,parent);
				this.dispose();
				mtd.setVisible(true);
            
            
            
            
            //this.dispose();
			} 
        }
	}


	@Override
	public void keyPressed(KeyEvent arg0) {
		
		
	}


	@Override
	public void keyReleased(KeyEvent e) {
		//numero
				if(e.getSource()==txtclenum){
					String text = txtclenum.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter(text,0)   );
			        }
					
				}
				////nom
				if(e.getSource()==txtcledate){
					String text = txtcledate.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,1)   );
			        }
					
				}
				////prenom
				if(e.getSource()==txtcletype){
					String text = txtcletype.getText();
			        if (text.length() == 0) {
			          filtre.setRowFilter(null);
			        } else {
			          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,2)   );
			        }
					
				}
				
	}


	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mousePressed(MouseEvent e2) {
		if(e2.getSource()==table){
			
			if (e2.getClickCount() == 2) {
	            JTable target = (JTable) e2.getSource();
	            int row = target.getSelectedRow();
	            //int column = target.getSelectedColumn();
	            Test t = listtests.get(row);
	            // you can play more here to get that cell value and all
	            new TestShowDialog(t).setVisible(true);
	            //this.dispose();
	           
	            //System.out.println("cccccccc");
	        }
			
			
			if (e2.getButton()== MouseEvent.BUTTON3){
				table.setRowSelectionAllowed(true);
				table.setColumnSelectionAllowed(false);
				table.changeSelection(e2.getY()/table.getRowHeight(),0,false, false); 
			} 
		}
		
	}


	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	private void ecrireFichier( File f, String nomFichier, String chemin ) throws IOException {
	    /* Pr�pare les flux. */
	    BufferedInputStream entree = null;
	    BufferedOutputStream sortie = null;
	    
//	    System.out.println(f.getPath());
//		System.out.println(f.getName());
//		System.out.println("nomFichier = "+nomFichier);
//		System.out.println("chemin = "+chemin);
	    try {
	        /* Ouvre les flux. */
	    	
	    	entree = new BufferedInputStream( new FileInputStream( f ), TAILLE_TAMPON );
	        sortie = new BufferedOutputStream( new FileOutputStream( new File( chemin + nomFichier ) ), TAILLE_TAMPON );
	        /*
	         * Lit le fichier re�u et �crit son contenu dans un fichier sur le
	         * disque.
	         */
	        byte[] tampon = new byte[TAILLE_TAMPON];
	        int longueur;
	        while ( ( longueur = entree.read( tampon ) ) > 0 ) {
	            sortie.write( tampon, 0, longueur );
	        }
	        JOptionPane.showMessageDialog(null, "Fichier Sauvegard�");
	    } 
	    finally {
	        try {

	            sortie.close();
	        } catch ( IOException ignore ) {ignore.printStackTrace();}
	        try {
	            entree.close();
	        } catch ( IOException ignore ) {ignore.printStackTrace();}
	    }
	}
	
	
}
