package medecin.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import dao.DAOUtilisateur;
import entities.Utilisateur;
import interfaces.BgBorder;
import interfaces.Login;

public class MedecinModifDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	JTextField textField_nom;
	JTextField textField_Prenom;
	JTextField textField_adresse;
	JTextField textField_tel;
	JTextField textField_email;
	JTextField textField_username;
	JTextField textField_password;
	
	Utilisateur u;
	DAOUtilisateur DAO;
	MedecinFrame parent;
	
	JButton btn_retour;
	JButton btn_appliquer;
	
	public MedecinModifDialog(Utilisateur utili, DAOUtilisateur daou, MedecinFrame par){
		
		DAO = daou;
		u = utili;
		parent = par;
		
		this.setTitle(u.getNom()+" "+u.getPrenom());
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
//		ImageIcon img = new ImageIcon(this.getClass().getResource("/images/"+u.getCin()+".jpg"));
//	    JLabel imagelabel = new JLabel(img);
//	    imagelabel.setBounds(500, 50, 300, 300);
//	    panel.add(imagelabel);
		
		///////////
		
	    JLabel lblNom = new JLabel("Nom: ");
	    lblNom.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblNom.setForeground(new Color(238, 203, 51));
	    lblNom.setBackground(Color.BLACK);
	    lblNom.setBounds(199, 50, 200, 22);
	    panel.add(lblNom);
	    textField_nom = new JTextField();
	    textField_nom.setText(u.getNom());
	    textField_nom.setBounds(400, 50, 200, 29);
	    panel.add(textField_nom);
		//
	    JLabel lblPrenom = new JLabel("Prenom: ");
	    lblPrenom.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblPrenom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblPrenom.setForeground(new Color(238, 203, 51));
	    lblPrenom.setBackground(Color.BLACK);
	    lblPrenom.setBounds(199, 80, 200, 22);
	    panel.add(lblPrenom);
	    textField_Prenom = new JTextField();
	    textField_Prenom.setText(u.getPrenom());
	    textField_Prenom.setBounds(400, 80, 200, 29);
	    panel.add(textField_Prenom);
	    
	    //
	    JLabel lblAdresse = new JLabel("Adresse: ");
	    lblAdresse.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblAdresse.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblAdresse.setForeground(new Color(238, 203, 51));
	    lblAdresse.setBackground(Color.BLACK);
	    lblAdresse.setBounds(199, 110, 200, 22);
	    panel.add(lblAdresse);
	    textField_adresse = new JTextField();
	    textField_adresse.setText(""+u.getAdresse());
	    textField_adresse.setBounds(400, 110, 200, 29);
	    panel.add(textField_adresse);
	    //
	    JLabel lblTelephone = new JLabel("T�l�phone: ");
	    lblTelephone.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblTelephone.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblTelephone.setForeground(new Color(238, 203, 51));
	    lblTelephone.setBackground(Color.BLACK);
	    lblTelephone.setBounds(199, 140, 200, 22);
	    panel.add(lblTelephone);
	    textField_tel = new JTextField();
	    textField_tel.setText(""+u.getTelephone());
	    textField_tel.setBounds(400, 140, 200, 29);
	    panel.add(textField_tel);
	   
	    //
	    JLabel lblEmail = new JLabel("E-mail: ");
	    lblEmail.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblEmail.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblEmail.setForeground(new Color(238, 203, 51));
	    lblEmail.setBackground(Color.BLACK);
	    lblEmail.setBounds(199, 170, 200, 22);
	    panel.add(lblEmail);
	    textField_email = new JTextField();
	    textField_email.setText(""+u.getEmail());
	    textField_email.setBounds(400, 170, 200, 29);
	    panel.add(textField_email);
	    //
	    JLabel lblUser_name = new JLabel("Username: ");
	    lblUser_name.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblUser_name.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblUser_name.setForeground(new Color(238, 203, 51));
	    lblUser_name.setBackground(Color.BLACK);
	    lblUser_name.setBounds(199, 200, 200, 22);
	    panel.add(lblUser_name);
	    textField_username = new JTextField();
	    
	    textField_username.setText(""+u.getUsername());
	    textField_username.setBounds(400, 200, 200, 29);
	    panel.add(textField_username);
	    ////
	    JLabel lblPassowrd = new JLabel("Passowrd: ");
	    lblPassowrd.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblPassowrd.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblPassowrd.setForeground(new Color(238, 203, 51));
	    lblPassowrd.setBackground(Color.BLACK);
	    lblPassowrd.setBounds(199, 230, 200, 22);
	    panel.add(lblPassowrd);
	    textField_password = new JTextField();
	    
	    textField_password.setText(""+u.getPassword());
	    textField_password.setBounds(400, 230, 200, 29);
	    panel.add(textField_password);
	    ////
		
	    //////
		btn_retour = new JButton("Retour");
		btn_retour.addActionListener(this);
		btn_retour.setForeground(Color.BLUE);
		btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_retour.setBounds(199, 353, 200, 29);
		panel.add(btn_retour);		
		//
		btn_appliquer = new JButton("Appliquer");
		btn_appliquer.addActionListener(this);
		btn_appliquer.setForeground(Color.BLUE);
		btn_appliquer.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_appliquer.setBounds(400, 353, 200, 29);
		panel.add(btn_appliquer);
		
		
		
		
		////
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn_retour){dispose();}
		if(e.getSource() == btn_appliquer){
			
			
			
			
			u.setNom(textField_nom.getText());
			u.setPrenom(textField_Prenom.getText());
			u.setAdresse(textField_adresse.getText());
			u.setTelephone(Integer.parseInt(textField_tel.getText()));
			u.setEmail(textField_email.getText());
			u.setUsername(textField_username.getText());
			u.setPassword(textField_password.getText());
			
			
			
			
			DAO.updateArbitre(u);
			parent.dispose();
			MedecinFrame mf = new MedecinFrame(u);
			mf.setVisible(true);
			
//			parent.validate();
//			parent.repaint();
			
			//parent.panel.revalidate();
			this.dispose();
			
		}
	}

}
