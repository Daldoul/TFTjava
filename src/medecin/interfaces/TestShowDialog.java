package medecin.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import entities.Test;
import interfaces.BgBorder;
import interfaces.Login;

public class TestShowDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	
	JButton btn_retour;
	
	
	Test test;
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	public TestShowDialog(Test t){
		this.test = t;
		this.setTitle("Test N� "+test.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
	
		
		JLabel lblType = new JLabel("Type: "+test.getType());
		lblType.setHorizontalAlignment(SwingConstants.LEFT);
		lblType.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lblType.setForeground(new Color(238, 203, 51));
		lblType.setBackground(Color.BLACK);
		lblType.setBounds(50, 50, 400, 22);
	    panel.add(lblType);
		//
	    JLabel lblDate = new JLabel("Date: "+t.getDate());
	    lblDate.setHorizontalAlignment(SwingConstants.LEFT);
	    lblDate.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblDate.setForeground(new Color(238, 203, 51));
	    lblDate.setBackground(Color.BLACK);
	    lblDate.setBounds(50, 80, 400, 22);
	    panel.add(lblDate);
	    //
	   
	  
		
	    	//////
				btn_retour = new JButton("Retour");
				btn_retour.addActionListener(this);
				btn_retour.setForeground(Color.BLUE);
				btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
				btn_retour.setBounds(300, 353, 200, 29);
			    panel.add(btn_retour);		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn_retour) {this.dispose();}

	}

}
