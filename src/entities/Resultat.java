package entities;

import java.util.Date;

public class Resultat {
	
	private int id; // not null 11
	private String description; // not null 100  
	private Date date;
	private String type; // not null 20
	private int id_user; // not null 11
	
	public Resultat() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date_debut) {
		this.date = date_debut;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	
	
	
	
}
