package entities;

public class Comp_user {
	
	private int id; // not null 11
	private int id_competition; // not null 11
	private int id_user; // not null 11
	
	public Comp_user() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_competition() {
		return id_competition;
	}

	public void setId_competition(int id_competition) {
		this.id_competition = id_competition;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	
	
	
}
