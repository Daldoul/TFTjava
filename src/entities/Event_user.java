package entities;

public class Event_user {
	
	private int id; // not null 11
	private int id_evenement; // not null 11
	private int id_user; // not null 11
	
	public Event_user() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_evenement() {
		return id_evenement;
	}

	public void setId_evenement(int id_evenement) {
		this.id_evenement = id_evenement;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	
	
	
}
