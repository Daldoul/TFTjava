package entities;

public class Pari {
	
	private int id; // not null 11
	private int joker; // not null 11
	private int id_user; // not null 11
	
	public Pari() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getJoker() {
		return joker;
	}

	public void setJoker(int joker) {
		this.joker = joker;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	
	
}
