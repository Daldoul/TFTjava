package filters;

import java.text.DecimalFormat;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

public class DayDocumentFilter extends DocumentFilter{
	
	private static char grpSymb;
	  static {
	    DecimalFormat df = (DecimalFormat) DecimalFormat.getInstance();
	    grpSymb = df.getDecimalFormatSymbols().getGroupingSeparator();
	  }

	  private boolean     allowGroupSymbols = true;

	  private String getFilteredText(String text) {
	    StringBuffer sb = new StringBuffer();
	    for (int i = 0; i < text.length(); i++) {
	      char c = text.charAt(i);
	      if (Character.isDigit(c))
	        sb.append(c);
	      else if (c == grpSymb) {
	        if (this.allowGroupSymbols)
	          sb.append(c);
	      } else
	        break;
	    }
	    return sb.toString();
	  }

	  
	  public void insertString(FilterBypass fb, int offset, String string,
	      AttributeSet attr) throws BadLocationException {
	    String ft = getFilteredText(string);
	    if (ft != null && ft.length() > 0)
	      super.insertString(fb, offset, ft, attr);
	  }

	  
	  public void replace(FilterBypass fb, int offset, int length, String text,
	      AttributeSet attrs) throws BadLocationException {
	    String ft = getFilteredText(text);
	    if (ft != null && ft.length() > 0)
	      super.replace(fb, offset, length, ft, attrs);
	  }

	  
	  public void setEditMode(boolean isEditMode) {
	    this.allowGroupSymbols = !isEditMode;
	  }
	
}
