package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Forma_user;

public class DAOForma_user {
	
	Statement s;
	ResultSet r;
		
	public DAOForma_user() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	
	
	public void ajouterForma_user(Forma_user u){
		try{
			s.executeUpdate("INSERT INTO forma_user (id_formation,id_user) VALUES ('"
														+u.getId_formation()+"','"
														+u.getId_user()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Forma_user>getForma_users() {
		List<Forma_user> lm = new ArrayList<Forma_user>();
		try{
			r=s.executeQuery(" select * from forma_user");
			while(r.next()){
				Forma_user u = new Forma_user();
				
				u.setId(r.getInt("id"));
				u.setId_formation(r.getInt("id_formation"));
				u.setId_user(r.getInt("id_user"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	public List<Forma_user>getForma_usersByFormaId(int id) {
		List<Forma_user> lm = new ArrayList<Forma_user>();
		try{
			r=s.executeQuery(" select * from forma_user where id_formation="+id+";");
			while(r.next()){
				Forma_user u = new Forma_user();
				
				u.setId(r.getInt("id"));
				u.setId_formation(r.getInt("id_formation"));
				u.setId_user(r.getInt("id_user"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	////
	
	public List<Forma_user>getForma_usersByUserId(int id) {
		List<Forma_user> lm = new ArrayList<Forma_user>();
		try{
			r=s.executeQuery(" select * from forma_user where id_user="+id+";");
			while(r.next()){
				Forma_user u = new Forma_user();
				
				u.setId(r.getInt("id"));
				u.setId_formation(r.getInt("id_formation"));
				u.setId_user(r.getInt("id_user"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	////
	public void deleteForma_user(Forma_user u){
		try{
			s.executeUpdate("DELETE FROM forma_user where id='"+u.getId()+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	
	public void deleteForma_userByArbitreIdFormationId(int id_forma, int id_arbitre){
		try{
			s.executeUpdate("DELETE FROM forma_user where id_formation='"+id_forma+"' and id_user='"+id_arbitre+"';");
		}catch (Exception e){e.printStackTrace();}
	}
	
	public void updateForma_user(Forma_user u){
		try{
			s.executeUpdate("update forma_user set id_formation= '"+u.getId_formation()
													+"' ,id_user='"+u.getId_user()
													+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
}
