package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Match;
import entities.Match_user;
import entities.Utilisateur;

public class DAOMatch_user {
	
	Statement s;
	ResultSet r;
		
	public DAOMatch_user() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	
	
	public void ajouterMatch_user(Match_user u){
		try{
			s.executeUpdate("INSERT INTO match_user (id_match,id_user) VALUES ('"
														+u.getId_match()+"','"
														+u.getId_user()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Match_user>getMatch_users() {
		List<Match_user> lm = new ArrayList<Match_user>();
		try{
			r=s.executeQuery(" select * from match_user");
			while(r.next()){
				Match_user u = new Match_user();
				
				u.setId(r.getInt("id"));
				u.setId_match(r.getInt("id_match"));
				u.setId_user(r.getInt("id_user"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	
	
	
	
	public void deleteMatch_user(Match_user u){
		try{
			s.executeUpdate("DELETE FROM match_user where id='"+u.getId()+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateMatch_user(Match_user u){
		try{
			s.executeUpdate("update match_user set id_match= '"+u.getId_match()
													+"' ,id_user='"+u.getId_user()
													+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	////////
	public void affectMatchGamer(Match match, Utilisateur gamer){
		try{
			s.executeUpdate("INSERT INTO match_user (id_match,id_user) VALUES ('"
														+match.getId()+"','"
														+gamer.getId()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
	}
	//////
	public void affectMatchArbitre(Match match, Utilisateur gamer){
		try{
			s.executeUpdate("INSERT INTO match_user (id_match,id_user) VALUES ('"
														+match.getId()+"','"
														+gamer.getId()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
	}
	/////
	
	public List<Match_user>getMatch_usersByMatchId(int id) {
		List<Match_user> lm = new ArrayList<Match_user>();
		try{
			r=s.executeQuery(" select * from match_user where id_match='"+id+"';");
			while(r.next()){
				Match_user u = new Match_user();
				
				u.setId(r.getInt("id"));
				u.setId_match(r.getInt("id_match"));
				u.setId_user(r.getInt("id_user"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	/////
	
	public List<Match_user>getMatch_usersByUserId(int id) {
		List<Match_user> lm = new ArrayList<Match_user>();
		try{
			r=s.executeQuery(" select * from match_user where id_user='"+id+"';");
			while(r.next()){
				Match_user u = new Match_user();
				
				u.setId(r.getInt("id"));
				u.setId_match(r.getInt("id_match"));
				u.setId_user(r.getInt("id_user"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	////
	public void deleteMatch_userByMatchUser(Match_user u){
		try{
			s.executeUpdate("DELETE FROM match_user where id_user='"+u.getId_user()+"' and id_match='"+u.getId_match()+"';");
		}catch (Exception e){e.printStackTrace();}
	}
	
}
