package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Match;
import entities.Stade;

public class DAOStade {
	
	Statement s;
	ResultSet r;
	List<Stade> listStade=new ArrayList<Stade>();
	
	public DAOStade() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	
	
	public void ajouterStade(Stade u){
		try{
			s.executeUpdate("INSERT INTO stade (nom,adresse,type) VALUES ('"
														+u.getNom()+"','"
														+u.getType()+"','"
														+u.getAdresse()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Stade>getStades() {
		List<Stade> lm = new ArrayList<Stade>();
		try{
			r=s.executeQuery(" select * from Stade");
			while(r.next()){
				Stade u = new Stade();
				
				u.setId(r.getInt("id"));
				u.setNom(r.getString("nom"));
				u.setAdresse(r.getString("adresse"));
				u.setType(r.getString("type"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	
	////
	public void deleteStade(Stade u, List<Match> listMatch){
		try{
			int t = listMatch.size();
			for(int i=0;i<t;i++){
				s.executeUpdate("DELETE FROM match_user where id='"+listMatch.get(i).getId()+"' ");
			}
			
			s.executeUpdate("DELETE FROM pidev2.match where lieux="+u.getId()+" ");
			s.executeUpdate("DELETE FROM stade where id='"+u.getId()+"' ;");
		}catch (Exception e){e.printStackTrace();}
	}
	////
	
	
	public void deleteStade(Stade u){
		try{
			s.executeUpdate("DELETE FROM stade where id='"+u.getId()+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateStade(Stade u){
		try{
			s.executeUpdate("update stade set nom= '"+u.getNom()
										+"' ,type= '"+u.getType()
										+"' ,adresse='"+u.getAdresse()
										+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
	public Stade getStadeById(int id){
		Stade u = new Stade();
		//System.out.println("get by id "+id);
		try{
			r=s.executeQuery(" select * from stade where id="+id);
			while(r.next()){
				
				
				u.setId(r.getInt("id"));
				u.setNom(r.getString("nom"));
				u.setAdresse(r.getString("adresse"));
				u.setType(r.getString("type"));
				
				System.out.println("get by id "+u.getId());
			}
				return u;
			}catch(Exception e){e.printStackTrace();return null;}
		
	}
	
}
