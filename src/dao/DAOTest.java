package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import entities.Test;

public class DAOTest {
	
	Statement s;
	ResultSet r;
		
	public DAOTest() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	SimpleDateFormat sdf_date = new java.text.SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf_time = new java.text.SimpleDateFormat("HH:mm:ss");
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public void ajouterTest(Test u){
		try{
			s.executeUpdate("INSERT INTO test (type,date) VALUES ('"
														+u.getType()+"','"
														+sdf.format(u.getDate())+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Test>getTests() {
		List<Test> lm = new ArrayList<Test>();
		try{
			r=s.executeQuery(" select * from test");
			while(r.next()){
				Test u = new Test();
				
				u.setId(r.getInt("id"));
				/////
				String st_date_debut = sdf_date.format(r.getDate("date"));
				String st_time_debut = sdf_time.format(r.getTime("date"));
				Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
				u.setDate(date_debut);
				//				
				
				u.setType(r.getString("type"));
				
				
				

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	//////////
	
	public Test getTestById(int id) {
		Test u = new Test();
		try{
			r=s.executeQuery(" select * from test where id='"+id+"';");
			while(r.next()){
				
				
				u.setId(r.getInt("id"));
				
				/////
				String st_date_debut = sdf_date.format(r.getDate("date"));
				String st_time_debut = sdf_time.format(r.getTime("date"));
				Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
				u.setDate(date_debut);
				//
				u.setType(r.getString("type"));
				
				
			}
				return u;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	///////////////
	
	
	public void deleteTest(Test u){
		try{
			s.executeUpdate("DELETE FROM test where id='"+u.getId()+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateTest(Test u){
		try{
			s.executeUpdate("update test set date= '"+sdf.format(u.getDate())
													+"' ,type='"+u.getType()
													+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	
}
