package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Ticket;

public class DAOTicket {
	
	Statement s;
	ResultSet r;
		
	public DAOTicket() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	
	
	public void ajouterTicket(Ticket u){
		try{
			s.executeUpdate("INSERT INTO ticket (prix,categorie,id_match,id_user) VALUES ('"
														+u.getPrix()+"','"
														+u.getCategorie()+"','"
														+u.getId_match()+"','"
														+u.getId_user()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Ticket>getTickets() {
		List<Ticket> lm = new ArrayList<Ticket>();
		try{
			r=s.executeQuery(" select * from ticket");
			while(r.next()){
				Ticket u = new Ticket();
				
				u.setId(r.getInt("id"));
				u.setPrix(r.getInt("prix"));
				u.setCategorie(r.getString("categorie"));
				u.setId_match(r.getInt("id_match"));
				u.setId_user(r.getInt("id_user"));
				
				

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	
	
	
	
	public void deleteTicket(Ticket u){
		try{
			s.executeUpdate("DELETE FROM ticket where id='"+u.getId()+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateTicket(Ticket u){
		try{
			s.executeUpdate("update ticket set prix= '"	+u.getPrix()
														+"' ,categorie= '"+u.getCategorie()
														+"' ,id_match='"+u.getId_match()
														+"' ,id_user='"+u.getId_user()
														+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	
}
