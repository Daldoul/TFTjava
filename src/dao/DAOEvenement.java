package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import entities.Evenement;
import entities.Match;

public class DAOEvenement {
	
	SimpleDateFormat sdf_date = new java.text.SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf_time = new java.text.SimpleDateFormat("HH:mm:ss");
	Statement s;
	ResultSet r;
		
	public DAOEvenement() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public void ajouterEvenement(Evenement u){
		try{
			s.executeUpdate("INSERT INTO evenement (lieux,nom,date_debut,date_fin) VALUES ('"
														+u.getLieux()+"','"
														+u.getNom()+"','"
														+sdf.format(u.getDate_debut())+"','"
														+sdf.format(u.getDate_fin())+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Evenement>getEvenements() {
		List<Evenement> lm = new ArrayList<Evenement>();
		try{
			r=s.executeQuery(" select * from evenement");
			while(r.next()){
				Evenement u = new Evenement();
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setNom(r.getString("nom"));
				
				/////
				String st_date_debut = sdf_date.format(r.getDate("date_debut"));
				String st_time_debut = sdf_time.format(r.getTime("date_debut"));
				Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
				u.setDate_debut(date_debut);
				//
				String st_date_fin = sdf_date.format(r.getDate("date_fin"));
				String st_time_fin = sdf_time.format(r.getTime("date_fin"));
				Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
				u.setDate_fin(date_fin);
				//////
				
				

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	
	
	
	
	public void deleteEvenement(Evenement u, List<Match> listMatch){
		try{
			int t = listMatch.size();
			for(int i=0;i<t;i++){
				s.executeUpdate("DELETE FROM match_user where id='"+listMatch.get(i).getId()+"' ");
			}
			
			
			
			
			s.executeUpdate("DELETE FROM pidev2.match where id_event="+u.getId()+" ");
			s.executeUpdate("DELETE FROM event_user where id_event='"+u.getId()+"' ");
			s.executeUpdate("DELETE FROM evenement where id='"+u.getId()+"' ;");
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateEvenement(Evenement u){
		try{
			s.executeUpdate("update evenement set lieux= '"+u.getLieux()
										+"' ,nom= '"+u.getNom()
										+"' ,date_debut='"+sdf.format(u.getDate_debut())
										+"' ,date_fin='"+sdf.format(u.getDate_fin())
										
										+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
	public Evenement getEventById(int id){
		Evenement u = new Evenement();
		try{
			r=s.executeQuery(" select * from evenement where id="+id);
			while(r.next()){
				
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setNom(r.getString("nom"));
				
				/////
				String st_date_debut = sdf_date.format(r.getDate("date_debut"));
				String st_time_debut = sdf_time.format(r.getTime("date_debut"));
				Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
				u.setDate_debut(date_debut);
				//
				String st_date_fin = sdf_date.format(r.getDate("date_fin"));
				String st_time_fin = sdf_time.format(r.getTime("date_fin"));
				Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
				u.setDate_fin(date_fin);
				//////
				
				

				
			}
				return u;
			}catch(Exception e){e.printStackTrace();return null;}
	}
}
