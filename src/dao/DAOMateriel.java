package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Materiel;

public class DAOMateriel {
	
	
	Statement s;
	ResultSet r;
		
	public DAOMateriel() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	
	
	public void ajouterMateriel(Materiel u){
		try{
			s.executeUpdate("INSERT INTO materiel (nom,quantite,id_user) VALUES ('"
														+u.getNom()+"','"
														+u.getQuantite()+"','"
														+u.getId_user()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Materiel>getMateriels() {
		List<Materiel> lm = new ArrayList<Materiel>();
		try{
			r=s.executeQuery(" select * from materiel");
			while(r.next()){
				Materiel u = new Materiel();
				
				u.setId(r.getInt("id"));
				u.setNom(r.getString("nom"));
				
				u.setQuantite(r.getInt("quantite"));
				u.setId_user(r.getInt("id_user"));
				
				

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	/////////
	public List<Materiel>getMaterielsByIdUser(int id) {
		List<Materiel> lm = new ArrayList<Materiel>();
		try{
			r=s.executeQuery(" select * from materiel where id_user='"+id+"';");
			while(r.next()){
				Materiel u = new Materiel();
				
				u.setId(r.getInt("id"));
				u.setNom(r.getString("nom"));
				
				u.setQuantite(r.getInt("quantite"));
				u.setId_user(r.getInt("id_user"));
				
				

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	///
	public void deleteMateriel(Materiel u){
		try{
			s.executeUpdate("DELETE FROM materiel where id='"+u.getId()+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateMateriel(Materiel u){
		try{
			s.executeUpdate("update materiel set nom= '"+u.getNom()
										+"' ,quantite= '"+u.getQuantite()
										+"' ,id_user='"+u.getId_user()
										
										+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
}
