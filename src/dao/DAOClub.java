package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Club;

public class DAOClub {
	
	Statement s;
	ResultSet r;
	List<Club> listUtilisateur=new ArrayList<Club>();
	
	public DAOClub() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	public List<Club>getClubs() { // Club 
		List<Club> l = new ArrayList<Club>();
		try{
			r=s.executeQuery(" select * from club ");
			while(r.next()){
				Club c1 = new Club();
				
				c1.setId(r.getInt("id"));
				c1.setNom(r.getString("nom"));
				c1.setVille(r.getString("ville"));
				
				l.add(c1);
			}
				return l;
			}catch(Exception e){e.printStackTrace();return null;}
	}//
	///
	public Club getClubById(int id) { // Club 
		Club c1 = new Club();
		try{
			r=s.executeQuery(" select * from club where id='"+id+"';");
			while(r.next()){
				
				
				c1.setId(r.getInt("id"));
				c1.setNom(r.getString("nom"));
				c1.setVille(r.getString("ville"));
				
				
			}
				return c1;
			}catch(Exception e){e.printStackTrace();return null;}
	}//
	///
	
	public int getClubByNom(String nomClub){
		int id = 0;
		try {
			r=s.executeQuery("select * from club where nom='"+nomClub+"'");
			r.next();
			id=r.getInt("id");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return id;
		
	}//
	
	
	
	
	public void ajouterClub(Club c){
		try{
			s.executeUpdate("INSERT INTO club (nom,ville) VALUES ('"
														+c.getNom()+"','"

														+c.getVille()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}////
	
	public void updateClub(Club c){
		try{
			s.executeUpdate("UPDATE club SET nom='"+c.getNom()+"' ,ville='"+c.getVille()+"' WHERE id='"+c.getId()+"';"   );
		}catch (Exception e){e.printStackTrace();}
		
	}////
	
	public void deleteClub(Club c){
		try{
			s.executeUpdate("delete from club where id='"+c.getId()+"' ;" );
		}catch (Exception e){e.printStackTrace();}
		
	}////
	
	
	////
}
