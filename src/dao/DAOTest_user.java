package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Test_user;

public class DAOTest_user {
	
	Statement s;
	ResultSet r;
		
	public DAOTest_user() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	
	
	public void ajouterTest_user(Test_user u){
		try{
			s.executeUpdate("INSERT INTO test_user (id_test,id_user) VALUES ('"
														+u.getId_test()+"','"
														+u.getId_user()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Test_user>getTest_users() {
		List<Test_user> lm = new ArrayList<Test_user>();
		try{
			r=s.executeQuery(" select * from test_user");
			while(r.next()){
				Test_user u = new Test_user();
				
				u.setId(r.getInt("id"));
				u.setId_test(r.getInt("id_test"));
				u.setId_user(r.getInt("id_user"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	//////
	
	public List<Test_user>getTest_usersByMedecinId(int id) {
		List<Test_user> lm = new ArrayList<Test_user>();
		try{
			r=s.executeQuery(" select * from test_user where id_user='"+id+"';");
			while(r.next()){
				Test_user u = new Test_user();
				
				u.setId(r.getInt("id"));
				u.setId_test(r.getInt("id_test"));
				u.setId_user(r.getInt("id_user"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	
	///////
	
	public List<Test_user>getTest_usersByTestId(int id) {
		List<Test_user> lm = new ArrayList<Test_user>();
		try{
			r=s.executeQuery(" select * from test_user where id_test='"+id+"';");
			while(r.next()){
				Test_user u = new Test_user();
				
				u.setId(r.getInt("id"));
				u.setId_test(r.getInt("id_test"));
				u.setId_user(r.getInt("id_user"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	///////
	public void deleteTest_user(Test_user u){
		try{
			s.executeUpdate("DELETE FROM test_user where id='"+u.getId()+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	///////
	
	public void deleteTest_usersByTestId(int id){
		try{
			s.executeUpdate("DELETE FROM test_user where id_test='"+id+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	///////
	public void deleteTest_userByIds(Test_user u){
		try{
			s.executeUpdate("DELETE FROM test_user where id_test='"+u.getId_test()+"' and id_user='"+u.getId_user()+"'; ");
		}catch (Exception e){e.printStackTrace();}
	}
	//////
	public void updateTest_user(Test_user u){
		try{
			s.executeUpdate("update test_user set id_test= '"+u.getId_test()
													+"' ,id_user='"+u.getId_user()
													+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
}
