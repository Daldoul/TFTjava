package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import entities.Formation;

public class DAOFormation {
	
	Statement s;
	ResultSet r;
		
	public DAOFormation() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat sdf_date = new java.text.SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf_time = new java.text.SimpleDateFormat("HH:mm:ss");
	public void ajouterFormation(Formation u){
		try{
			s.executeUpdate("INSERT INTO formation (lieux,nom,date_ouverture,date_clouture,type) VALUES ('"
														+u.getLieux()+"','"
														+u.getNom()+"','"
														+sdf.format(u.getDate_ouverture())+"','"
														+sdf.format(u.getDate_cloture())+"','"
														+u.getType()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Formation>getFormations() {
		List<Formation> lm = new ArrayList<Formation>();
		try{
			r=s.executeQuery(" select * from formation");
			while(r.next()){
				Formation u = new Formation();
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setNom(r.getString("nom"));
				
			/////
			String st_date_debut = sdf_date.format(r.getDate("date_ouverture"));
			String st_time_debut = sdf_time.format(r.getTime("date_ouverture"));
			Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
			u.setDate_ouverture(date_debut);
			//
			String st_date_fin = sdf_date.format(r.getDate("date_clouture"));
			String st_time_fin = sdf_time.format(r.getTime("date_clouture"));
			Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
			u.setDate_cloture(date_fin);
			//////
				
				u.setType(r.getString("nom"));

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	////
	
	public Formation getFormationById(int id) {
		Formation u = new Formation();
		try{
			r=s.executeQuery(" select * from formation where id='"+id+ "';");
			while(r.next()){
				
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setNom(r.getString("nom"));
				
				/////
				String st_date_debut = sdf_date.format(r.getDate("date_ouverture"));
				String st_time_debut = sdf_time.format(r.getTime("date_ouverture"));
				Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
				u.setDate_ouverture(date_debut);
				//
				String st_date_fin = sdf_date.format(r.getDate("date_clouture"));
				String st_time_fin = sdf_time.format(r.getTime("date_clouture"));
				Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
				u.setDate_cloture(date_fin);
				//////
				
				u.setType(r.getString("nom"));

				
			}
				return u;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	////
	public void deleteFormation(Formation u){
		try{
			s.executeUpdate("DELETE FROM forma_user where id_formation='"+u.getId()+"' ");
			s.executeUpdate("DELETE FROM formation where id='"+u.getId()+"' ");
			
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateFormation(Formation u){
		try{
			s.executeUpdate("update formation set lieux= '"+u.getLieux()
										+"' ,nom= '"+u.getNom()
										+"' ,type= '"+u.getType()
										+"' ,date_ouverture='"+sdf.format(u.getDate_ouverture())
										+"' ,date_clouture='"+sdf.format(u.getDate_cloture())
										
										+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
}
