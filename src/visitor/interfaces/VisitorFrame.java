package visitor.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import entities.Utilisateur;
import interfaces.BgBorder;
import interfaces.Login;

public class VisitorFrame extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	JButton btn_club;
	JButton btn_Gamer;
	JButton btn_Match;
	JButton btn_Exit;
	

	public VisitorFrame(Utilisateur u1) {
		this.setTitle("TFT Visiteur - "+u1.getNom()+" "+u1.getPrenom());
		this.setResizable(false);
		this.setBounds(100, 100, 1000, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1000, 600);
		this.getContentPane().add(panel);
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis003.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		
		JPanel panel_1 = new JPanel();
	     panel_1.setBorder(new LineBorder(new Color(204, 255, 255), 4));
	     panel_1.setBackground(new Color(0, 102, 153));
	     panel_1.setBounds(200, 60, 600, 70);
	     panel.add(panel_1);
	     panel_1.setLayout(null);
	     
	     JLabel lblInscri = new JLabel("Espace Visiteurs de TFT - Bienvenue");
	     lblInscri.setHorizontalAlignment(SwingConstants.CENTER);
	     lblInscri.setForeground(Color.WHITE);
	     lblInscri.setFont(new Font("Lucida Blackletter", Font.PLAIN, 26));
	     lblInscri.setBounds(6, 6, 590, 58);
	     panel_1.add(lblInscri);
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
	     
			
	     btn_club = new JButton("Nos Clubs");
	     btn_club.addActionListener(this);
	     btn_club.setForeground(Color.BLUE);
	     btn_club.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	     btn_club.setBounds(198, 271, 200, 29);
		 panel.add(btn_club);
		     
		    btn_Gamer = new JButton("Nos Joueurs");
		    btn_Gamer.addActionListener(this);
		    btn_Gamer.setForeground(Color.BLUE);
		    btn_Gamer.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		    btn_Gamer.setBounds(399, 271, 200, 29);
		    panel.add(btn_Gamer);
		    
		    
		    btn_Match = new JButton("Nos Matchs");
		    btn_Match.addActionListener(this);
		    btn_Match.setForeground(Color.BLUE);
		    btn_Match.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		    btn_Match.setBounds(600, 271, 200, 29);
		    panel.add(btn_Match);
	     
		    btn_Exit = new JButton("Quitter");
		    btn_Exit.addActionListener(this);
		    btn_Exit.setForeground(Color.BLUE);
		    btn_Exit.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		    btn_Exit.setBounds(750, 519, 200, 29);
		    panel.add(btn_Exit);
	     
	     
	     
	     
	     
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		if (ev.getSource() == btn_Exit){
			int choix = JOptionPane.showConfirmDialog(null,"Voulez Vous Vraiment Quitter ?","Quitter", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);if ( choix == JOptionPane.OK_OPTION  ){System.exit(1);}
		}
		if (ev.getSource() == btn_club){
			VisitorClubDialog vcDialog = new VisitorClubDialog();
			vcDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Gamer){
			VisitorGamerDialog vgDialog = new VisitorGamerDialog();
			vgDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Match){
			VisitorMatchDialog vmDialog = new VisitorMatchDialog();
			vmDialog.setVisible(true);
		}

	}

}
