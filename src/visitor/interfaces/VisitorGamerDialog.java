package visitor.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import admin.interfaces.gamer.GamerShowDialog;
import dao.DAOClub;
import dao.DAOUtilisateur;
import entities.Club;
import entities.Utilisateur;
import filters.NumericAndLengthFilter;
import interfaces.BgBorder;
import interfaces.Login;

public class VisitorGamerDialog extends JDialog implements ActionListener, KeyListener, MouseListener {
	private static final long serialVersionUID = 1L;

	JButton btn_retour;
	
	JTextField txtclenom;
	JTextField txtcleprenom;
	
	JTextField txtcleclub;
	
	
	DAOUtilisateur daou ;
	List<Utilisateur> listUtilisateur;
	
	String col[] = {"Nom", "Prenom" , "Club" , "image"};
	JTable table ;
	DefaultTableModel model;
	TableRowSorter<TableModel> filtre;
	DAOClub daoc = new DAOClub();
	
	JMenuItem ouvrirItem;
    
    NumericAndLengthFilter numericandlengthfilter = new NumericAndLengthFilter(11);
    	
	@SuppressWarnings("serial")
	public VisitorGamerDialog (){
		
		this.setTitle("Les Joueurs");
		this.setResizable(false);
		this.setBounds(100, 100, 1000, 700);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1000, 700);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis04.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		daou = new DAOUtilisateur();
		
		listUtilisateur = daou.getGamers();
		int t = listUtilisateur.size();
		
	    Object data[][] = new Object[t][4];
	    
	      for(int i = 0;i<t;i++ ){
	        data[i][0] = ""+listUtilisateur.get(i).getNom();
	        data[i][1] = ""+listUtilisateur.get(i).getPrenom();
	        
	        if(listUtilisateur.get(i).getId_club() == 0){data[i][2] = "Libre";}
	        else{ Club c = daoc.getClubById(listUtilisateur.get(i).getId_club());  data[i][2] =  c.getNom();}
	        
	         
	        
	        
	        
	        System.out.println("le champ image : "+listUtilisateur.get(i).getImage()+".jpg");
	        
	       
	        if(listUtilisateur.get(i).getImage()!= null){try {
				data[i][3] = new ImageIcon(ImageIO.read(new File("C:/imagesUtilisateurs/"+listUtilisateur.get(i).getCin()+".jpg")));
			} catch (IOException e) {
				e.printStackTrace();
			}}
	        else {data[i][3] = "Pas l'image";}
	      }
        
        model = new DefaultTableModel(data, col){
	    	  /*
	    	  @SuppressWarnings({ "unchecked", "rawtypes" })
			public Class getColumnClass(int column) {
	    	        Class returnValue;
	    	        if ((column >= 0) && (column < getColumnCount())) {
	    	          returnValue = getValueAt(0, column).getClass();
	    	        } else {
	    	          returnValue = Object.class;
	    	        }
	    	        return returnValue;
	    	      }
	    	  */
	      };
	      table = new JTable(model){
	    	  	
	              @SuppressWarnings({ "unchecked", "rawtypes" })
				public Class getColumnClass(int column){return getValueAt(0, column).getClass();}
	              public boolean isCellEditable(int row, int column) {return false;}
	              
	      };

	      table.setRowHeight(80);
	      table.setCellSelectionEnabled(false);
	      table.setFocusable(false);
	      table.setRowSelectionAllowed(true);
//	      table.setWrapStyleWord(true);
//        table.setLineWrap(true);
	      table.setShowGrid(true);
	      table.setShowVerticalLines(true);
	      
	      
	      filtre = new TableRowSorter<TableModel>(model);
	      
	      
	      table.setRowSorter(filtre);
	      
	      table.addMouseListener(this);
	      //table.addAncestorListener(this);
	      
	      JScrollPane panJtab = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	      //panJtab.setPreferredSize(new Dimension(1240, 550));
	      panJtab.setBounds(15, 50, 970, 570);
	      
	      panel.add(panJtab); 
	      
		    
		// les recherches
		
	    
	  	txtclenom = new JTextField();
	  	txtclenom.addKeyListener(this);
	  	txtclenom.setBounds(15, 21, 96, 29);
		panel.add(txtclenom);
		txtclenom.setColumns(20);
	  	
	  	txtcleprenom = new JTextField();
	  	txtcleprenom.addKeyListener(this);
	  	txtcleprenom.setBounds(206, 21, 95, 29);
		panel.add(txtcleprenom);
		txtcleprenom.setColumns(20);
	  	
		txtcleclub = new JTextField();
		txtcleclub.addKeyListener(this);
	  	txtcleclub.setBounds(301, 21, 94, 29);
		panel.add(txtcleclub);
		txtcleclub.setColumns(11);
	  	
		
		/////
		
		
		
		
		//////
		btn_retour = new JButton("Retour");
		btn_retour.addActionListener(this);
		btn_retour.setForeground(Color.BLUE);
		btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_retour.setBounds(750, 630, 200, 29);
	    panel.add(btn_retour);
		
		
	 // Create popup menu, attach popup menu listener
	    JPopupMenu popupMenu = new JPopupMenu("Menu");
	    
	    ouvrirItem = new JMenuItem("Ouvrir");ouvrirItem.addActionListener(this);
	    popupMenu.add(ouvrirItem);
	    
	    
	    
	    table.setComponentPopupMenu(popupMenu);
		
		
	    
	    
	    
	    
		
	}
	public void rebuild(){
		SwingUtilities.updateComponentTreeUI(this);
		model.fireTableDataChanged();
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e1) {
		if (e1.getSource() == btn_retour) {dispose();}
		
		if (e1.getSource() == ouvrirItem) {
            int row = table.getSelectedRow();
            //int column = table.getSelectedColumn();
            
            Utilisateur u = listUtilisateur.get(row);
            
            // you can play more here to get that cell value and all
            new GamerShowDialog(u).setVisible(true);
            //this.dispose();
            
        }
		
		
		
		
		

	}


	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mousePressed(MouseEvent e2) {
		if(e2.getSource()==table){
			
			if (e2.getClickCount() == 2) {
	            JTable target = (JTable) e2.getSource();
	            int row = target.getSelectedRow();
	            //int column = target.getSelectedColumn();
	            Utilisateur u = listUtilisateur.get(row);
	            // you can play more here to get that cell value and all
	            new VisitorGamerShowDialog(u).setVisible(true);
	            //this.dispose();
	           
	            //System.out.println("cccccccc");
	        }
			
			
			if (e2.getButton()== MouseEvent.BUTTON3){
				table.setRowSelectionAllowed(true);
				table.setColumnSelectionAllowed(false);
				table.changeSelection(e2.getY()/table.getRowHeight(),0,false, false); 
			} 
		}
		
	}


	@Override
	public void mouseReleased(MouseEvent e) {
		
		
	}


	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void keyReleased(KeyEvent e) {
		
		////nom
		if(e.getSource()==txtclenom){
			String text = txtclenom.getText();
	        if (text.length() == 0) {
	          filtre.setRowFilter(null);
	        } else {
	          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,1)   );
	        }
			
		}
		////prenom
		if(e.getSource()==txtcleprenom){
			String text = txtcleprenom.getText();
	        if (text.length() == 0) {
	          filtre.setRowFilter(null);
	        } else {
	          filtre.setRowFilter(RowFilter.regexFilter("(?i)"+text,2)   );
	        }
			
		}
		/////cin
		if(e.getSource()==txtcleclub){
			String text = txtcleclub.getText();
	        if (text.length() == 0) {
	          filtre.setRowFilter(null);
	        } else {
	          filtre.setRowFilter(RowFilter.regexFilter(text,3)   );
	        }
			
		}
		
	
	}
	
	    
	
}
