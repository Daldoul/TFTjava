package arbitre.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import dao.DAOUtilisateur;
import entities.Utilisateur;
import interfaces.BgBorder;
import interfaces.Login;
import outils.FileChooser2;

public class ArbitreFrame extends JFrame implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	JButton btn_Profil;
	JButton btn_Match;
	JButton btn_Materiel;
	JButton btn_Formation;
	JButton btn_Inscrir;
	JButton btn_Image;
	JButton btn_Exit;
	
	DAOUtilisateur daoa;
	Utilisateur arbitre;
	
	File f1;
	File dir;
	
	public static final int TAILLE_TAMPON = 10240; // 10 ko
	
	String chemin = "C:/imagesUtilisateurs/";
	ImageIcon iconExc = new ImageIcon(getClass().getResource("/images/Exception.png"));
	ImageIcon iconWar = new ImageIcon(getClass().getResource("/images/ERROR35.png"));
	ImageIcon iconSucc = new ImageIcon(getClass().getResource("/images/Succes.png"));

	public ArbitreFrame(Utilisateur ut){
		arbitre=ut;
		daoa = new DAOUtilisateur();
		
		this.setTitle("TFT Arbitre - "+arbitre.getNom()+" "+arbitre.getPrenom());
		this.setResizable(false);
		this.setBounds(100, 100, 1000, 600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1000, 600);
		this.getContentPane().add(panel);
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis003.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		
		JPanel panel_1 = new JPanel();
	     panel_1.setBorder(new LineBorder(new Color(204, 255, 255), 4));
	     panel_1.setBackground(new Color(0, 102, 153));
	     panel_1.setBounds(200, 60, 600, 70);
	     panel.add(panel_1);
	     panel_1.setLayout(null);
	     
	     JLabel lblInscri = new JLabel("Espace Arbitres de TFT - Bienvenue");
	     lblInscri.setHorizontalAlignment(SwingConstants.CENTER);
	     lblInscri.setForeground(Color.WHITE);
	     lblInscri.setFont(new Font("Lucida Blackletter", Font.PLAIN, 26));
	     lblInscri.setBounds(6, 6, 590, 58);
	     panel_1.add(lblInscri);
	     
	     
	     
	     	JLabel lblNom = new JLabel("Nom et Pr�nom: "+arbitre.getNom()+" "+arbitre.getPrenom());
		    lblNom.setHorizontalAlignment(SwingConstants.LEFT);
		    lblNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		    lblNom.setForeground(Color.BLUE);
		    lblNom.setBackground(Color.BLACK);
		    lblNom.setBounds(100, 150, 400, 22);
		    panel.add(lblNom);
			
		    //
		    JLabel lblCin = new JLabel("N�CIN: "+arbitre.getCin());
		    lblCin.setHorizontalAlignment(SwingConstants.LEFT);
		    lblCin.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		    lblCin.setForeground(Color.BLUE);
		    lblCin.setBackground(Color.BLACK);
		    lblCin.setBounds(100, 180, 400, 22);
		    panel.add(lblCin);
		    //
		    JLabel lblAdresse = new JLabel("Adresse: "+arbitre.getAdresse());
		    lblAdresse.setHorizontalAlignment(SwingConstants.LEFT);
		    lblAdresse.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		    lblAdresse.setForeground(Color.BLUE);
		    lblAdresse.setBackground(Color.BLACK);
		    lblAdresse.setBounds(100, 210, 400, 22);
		    panel.add(lblAdresse);
		    //
		    JLabel lblTelephone = new JLabel("T�l�phone: "+arbitre.getTelephone());
		    lblTelephone.setHorizontalAlignment(SwingConstants.LEFT);
		    lblTelephone.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		    lblTelephone.setForeground(Color.BLUE);
		    lblTelephone.setBackground(Color.BLACK);
		    lblTelephone.setBounds(100, 240, 400, 22);
		    panel.add(lblTelephone);
		    //
		    JLabel lblGrade = new JLabel("Grade: "+arbitre.getGrade());
		    lblGrade.setHorizontalAlignment(SwingConstants.LEFT);
		    lblGrade.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		    lblGrade.setForeground(Color.BLUE);
		    lblGrade.setBackground(Color.BLACK);
		    lblGrade.setBounds(100, 270, 400, 22);
		    panel.add(lblGrade);
		    //
		    JLabel lblEmail = new JLabel("E-mail: "+arbitre.getEmail());
		    lblEmail.setHorizontalAlignment(SwingConstants.LEFT);
		    lblEmail.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		    lblEmail.setForeground(Color.BLUE);
		    lblEmail.setBackground(Color.BLACK);
		    lblEmail.setBounds(100, 300, 400, 22);
		    panel.add(lblEmail);
		    //
		    JLabel lblUser_name = new JLabel("Username: "+arbitre.getUsername());
		    lblUser_name.setHorizontalAlignment(SwingConstants.LEFT);
		    lblUser_name.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		    lblUser_name.setForeground(Color.BLUE);
		    lblUser_name.setBackground(Color.BLACK);
		    lblUser_name.setBounds(100, 330, 400, 22);
		    panel.add(lblUser_name);
		    //
		    JLabel lblPassword = new JLabel("Password: "+arbitre.getPassword());
		    lblPassword.setHorizontalAlignment(SwingConstants.LEFT);
		    lblPassword.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		    lblPassword.setForeground(Color.BLUE);
		    lblPassword.setBackground(Color.BLACK);
		    lblPassword.setBounds(100, 360, 400, 22);
		    panel.add(lblPassword);
		    //
		    JLabel lblDescription = new JLabel("Description: "+arbitre.getDescription());
		    lblDescription.setHorizontalAlignment(SwingConstants.LEFT);
		    lblDescription.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		    lblDescription.setForeground(Color.BLUE);
		    lblDescription.setBackground(Color.BLACK);
		    lblDescription.setBounds(100, 390, 400, 22);
		    panel.add(lblDescription);
	     
	     
		    ImageIcon img = null;
			try {
				img = new ImageIcon(ImageIO.read(new File("C:/imagesUtilisateurs/"+arbitre.getCin()+".jpg")));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    if(img!=null){
		    	JLabel imagelabel = new JLabel(img);
			    imagelabel.setBounds(550, 150, 300, 300);
			    panel.add(imagelabel);
		    }
		    else{
		    	JLabel imagelabel = new JLabel("Pas d'image");
			    imagelabel.setBounds(550, 150, 300, 300);
			    panel.add(imagelabel);
		    }
	     
	     
	     
	     
	     
	     
	     
	     
	     btn_Profil = new JButton("Profil");
	     btn_Profil.addActionListener(this);
	     btn_Profil.setForeground(Color.BLUE);
	     btn_Profil.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	     btn_Profil.setBounds(98, 470, 99, 29);
		 panel.add(btn_Profil);
		     
		 btn_Match = new JButton("Mes Matchs");
		 btn_Match.addActionListener(this);
		 btn_Match.setForeground(Color.BLUE);
		 btn_Match.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		 btn_Match.setBounds(299, 470, 200, 29);
		 panel.add(btn_Match);
		    
		    
		 btn_Materiel = new JButton("Mes Materiels");
		 btn_Materiel.addActionListener(this);
		 btn_Materiel.setForeground(Color.BLUE);
		 btn_Materiel.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		 btn_Materiel.setBounds(500, 470, 200, 29);
		 panel.add(btn_Materiel);
		 
		 btn_Formation = new JButton("Formations");
		 btn_Formation.addActionListener(this);
		 btn_Formation.setForeground(Color.BLUE);
		 btn_Formation.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		 btn_Formation.setBounds(701, 470, 110, 29);
		 panel.add(btn_Formation);	
		 
		 btn_Inscrir = new JButton("Inscrire");
		 btn_Inscrir.addActionListener(this);
		 btn_Inscrir.setForeground(Color.BLUE);
		 btn_Inscrir.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		 btn_Inscrir.setBounds(812, 470, 99, 29);
		 panel.add(btn_Inscrir);	
		 
		 btn_Image = new JButton("Photo");
		 btn_Image.addActionListener(this);
		 btn_Image.setForeground(Color.BLUE);
		 btn_Image.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		 btn_Image.setBounds(198, 470, 100, 29);
		 panel.add(btn_Image);
	     
	     
	     
	     
		 btn_Exit = new JButton("Quitter");
		 btn_Exit.addActionListener(this);
		 btn_Exit.setForeground(Color.BLUE);
		 btn_Exit.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		 btn_Exit.setBounds(750, 519, 200, 29);
		 panel.add(btn_Exit);
	}

	@Override
	public void actionPerformed(ActionEvent ev) {
		
		if (ev.getSource() == btn_Exit){
			int choix = JOptionPane.showConfirmDialog(null,"Voulez Vous Vraiment Quitter ?","Quitter", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);if ( choix == JOptionPane.OK_OPTION  ){System.exit(1);}
		}

		
		if (ev.getSource() == btn_Profil){
			ArbitreModifDialog gamerModifDialog = new ArbitreModifDialog(arbitre, daoa, this);
			gamerModifDialog.setVisible(true);
		}
		
		if (ev.getSource() == btn_Image){
			FileChooser2 fc = new FileChooser2(this);
			if(fc.getSelectedFile()!=null){
			f1=fc.getSelectedFile();
			
			dir=fc.getCurrentDirectory();
			

				try {
					ecrireFichier(f1, arbitre.getCin()+".jpg", chemin);
				} catch (IOException e) {
					JOptionPane.showMessageDialog(null,"Erreur..", "erreur fichier image ",JOptionPane.INFORMATION_MESSAGE, iconSucc);
					e.printStackTrace();}
			
			
			ArbitreFrame af = new ArbitreFrame(arbitre);
			
			af.setVisible(true);
			this.dispose();
		}
		}
		if (ev.getSource() == btn_Match){
			ArbitreMatchDialog amDialog = new ArbitreMatchDialog(arbitre,daoa,this);
			amDialog.setVisible(true);
		}
		
		
		
		
		if (ev.getSource() == btn_Formation){
			ArbitreFormationDialog gfDialog = new ArbitreFormationDialog(arbitre,daoa,this);
			gfDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Inscrir){
			ArbitreFormationInscrirDialog gfDialog = new ArbitreFormationInscrirDialog(arbitre,daoa,this);
			gfDialog.setVisible(true);
		}
		if (ev.getSource() == btn_Materiel){
			ArbitreMaterielDialog gmDialog = new ArbitreMaterielDialog(arbitre,daoa,this);
			gmDialog.setVisible(true);
		}
		
		
		
		
	
	}	
		private void ecrireFichier( File f, String nomFichier, String chemin ) throws IOException {
		    /* Pr�pare les flux. */
		    BufferedInputStream entree = null;
		    BufferedOutputStream sortie = null;
		    
		    System.out.println(f.getPath());
			System.out.println(f.getName());
			System.out.println("nomFichier = "+nomFichier);
			System.out.println("chemin = "+chemin);
		    try {
		        /* Ouvre les flux. */
		    	
		    	entree = new BufferedInputStream( new FileInputStream( f ), TAILLE_TAMPON );
		        sortie = new BufferedOutputStream( new FileOutputStream( new File( chemin + nomFichier ) ), TAILLE_TAMPON );
		        /*
		         * Lit le fichier re�u et �crit son contenu dans un fichier sur le
		         * disque.
		         */
		        byte[] tampon = new byte[TAILLE_TAMPON];
		        int longueur;
		        while ( ( longueur = entree.read( tampon ) ) > 0 ) {
		            sortie.write( tampon, 0, longueur );
		        }
		    } 
		    finally {
		        try {

		            sortie.close();
		        } catch ( IOException ignore ) {ignore.printStackTrace();}
		        try {
		            entree.close();
		        } catch ( IOException ignore ) {ignore.printStackTrace();}
		    }
		}
}
